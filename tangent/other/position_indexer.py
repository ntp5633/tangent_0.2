from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import json
import itertools
import jsonpickle
import os
from tangent.common.hits_generation.modified_hit_generator import SentenceGraphFragmentGenerator, SentenceGraph
from tangent.other.position_mysql_index import PostionMySQLIndex

from sys import argv, exit


def print_help_and_exit():
    """
    Prints usage statement
    """

    exit('Usage: python postion_indexer.py doc_id_mapping collection')


def posIndexerHelper(pargs):
    database_collection, mappings = pargs
    name = os.getpid()

    r = PostionMySQLIndex(collection=database_collection)
    print("Process %s has %s elements to process" % (name, len(mappings)))

    docs_indexed = 0
    for (path, doc_id, title) in mappings:
        sentence_graph = SentenceGraphFragmentGenerator.generate_sentence_graph(path)
        assert isinstance(sentence_graph, SentenceGraph)
        pickled_graph = jsonpickle.encode(sentence_graph)
        r.add_document(doc_id, pickled_graph)
        docs_indexed = docs_indexed + 1

        if docs_indexed % 50 == 0:
            print(" Process %s Indexed %s docs " % (name, docs_indexed ))

    print(" Process %s final commit to database" % (name))
    r.commit_all()
    return name


def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in range(0, len(l), n):
        yield l[i:i + n]


if __name__ == '__main__':
    if len(argv) == 3:
        collection = argv[1]
        doc_id_mapping_path = argv[2]

        with open(doc_id_mapping_path) as mapping_file:
            mappings = []
            mappings = json.load(mapping_file)

        num_docs=len(mappings)
        print("There are " + str(num_docs) + " documents to index");

        r = PostionMySQLIndex(collection=collection)
        r.flush()

        max_jobs = 4
        chunk_size = 50
        all_jobs = []
        num_docs_remaining=num_docs
        args = [(collection, small_chunk) for small_chunk in chunks(mappings, chunk_size)]
        with ProcessPoolExecutor(max_workers=max_jobs) as executor:

            for pid in executor.map(posIndexerHelper, args):
                print("%s is done" % (pid))
                num_docs_remaining=num_docs_remaining-chunk_size
                print("Remaing docs %s" % (num_docs_remaining))

    else:
        print_help_and_exit()
