from collections import namedtuple
import string
from bs4 import BeautifulSoup, BeautifulStoneSoup
import sys
from tangent.math.mathml.latex_mml import LatexToMathML, ServerLatexToMathML


__author__ = 'Nidhin'

import re
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup

class MathExtractorOutput:
    def __init__(self, cleaned_file_content="", latex_mathml_dict={}, unparsed_expressions=[], math_token_positions=[],
                 just_latex=[]):
        self.cleaned_file_content = cleaned_file_content
        self.latex_mathml_dict = latex_mathml_dict
        self.unparsed_expressions = unparsed_expressions
        self.math_token_positions = math_token_positions

        self.just_latex = just_latex


class MathExtractor:
    def __init__(self):
        pass

    replace_math_pattern = u"<math.*?>.*?</math>|\\$+.{1,200}?\\$+"
    replace_math_pattern = re.compile(replace_math_pattern, re.DOTALL)

    split_pattern = u"<math.*?>.*?</math>|\\$+.{1,200}?\\$+|[^<\\s]+"
    # split_pattern = u"\$math_token_start(.*?)math_token_end\$|"
    split_pattern = re.compile(split_pattern, re.DOTALL)


    @classmethod
    def get_string_tokenized(cls, content):
        return cls.split_pattern.findall(content)

    @classmethod
    def math_token_positions(cls, content):

        tokens = cls.split_pattern.findall(content)

        token_positions = {}

        for idx, token in enumerate(tokens):


            if token.startswith(u"<math>") and token.endswith(u"</math>"):
                token = token.replace(u"<math>", "")
                token = token.replace(u"</math>", "")
                token = token.strip()
                token_positions[token] = token_positions.get(token, [])
                token_positions[token].append(idx)

        return token_positions



    @classmethod
    def __reg_cleaner(cls,latex_mathml, error_expressions):
        def reg_cleaner_helper(content):

            # if dollar sign


            if content.group(0).startswith("$") and content.group(0).endswith("$"):
                node_text = content.group(0)

                # verify if math expression
                occurences_of_slash = node_text.count('\'')

                if len(node_text) > 15 and occurences_of_slash < 2:
                    return ""
                else:
                    node_text = node_text.replace("$", "")
                    latex_mathml[node_text] = None
                    return u"<!--<math>" + node_text + u"</math>-->"

            #try:
            #    tree = ET.ElementTree(ET.fromstring(content.group(0)))
            #except Exception as e:
            #    error_expressions.append(content.group(0))
            #    return ""  # if error return empty string

            parsed_xml=BeautifulSoup(content.group(0))
            math_root=parsed_xml.find("math")
            altext=math_root.get("alttext")
            application_tex= math_root.find("annotation",{"encoding":"application/x-tex"})

            if application_tex is not None:
                application_tex_text=application_tex.text
                application_tex.decompose()


            latex=altext

            if latex is None:
                latex=application_tex_text

            pmml_markup=math_root.find("annotation-xml",{"encoding":"MathML-Presentation"})
            cmml_markup=math_root.find("annotation-xml",{"encoding":"MathML-Content"})


            if cmml_markup is not None:
                cmml_markup.decompose()
                if pmml_markup is None:
                    pmml_markup=math_root

            if latex is not None: # if there is latex
                if pmml_markup is None:#if no presentattion markup
                    latex_mathml[latex]=None

                else:#bind latex to mathml
                    latex_mathml[latex] = str(pmml_markup)
                return u"<!-- <math>" + latex + u"</math> -->"
            else:
                if pmml_markup is not None:
                    latex_mathml['no_latex'] = latex_mathml.get('no_latex', [])
                    latex_mathml['no_latex'].append(str(pmml_markup))
                else:
                    node_text = math_root.text
                    latex_mathml[node_text] = None
                    return u"<!-- <math>" + node_text + u"</math> -->"




            """

            if application_tex:
                application_tex=application_tex.text

            if root.attrib != None:  # has attributes
                alttext = tree.getroot().get("alttext")
                alt_text_node = tree.find(
                    ".//{http://www.w3.org/1998/Math/MathML}semantics//{http://www.w3.org/1998/Math/MathML}annotation[@encoding='application/x-tex']")

                if alttext or alt_text_node is not None:
                    if alt_text_node is not None:
                        alttext = alt_text_node.text

                    alttext = alttext.strip()
                    latex_mathml[alttext] = content.group(0)

                    return u"<!-- <math>" + alttext + u"</math> -->"
                else:
                    latex_mathml['no_latex'] = latex_mathml.get('no_latex', [])
                    latex_mathml['no_latex'].append(content.group(0))
                    # return u"\n$math_token_start " + alttext + u" math_token_end$ \n"

            else:  # assume it is tex tag within
                node_text = tree.getroot().text
                latex_mathml[node_text] = None
                return u"<!-- <math>" + node_text + u"</math> -->"
            """

        return reg_cleaner_helper


    @classmethod
    def input_cleaner(cls, content) -> MathExtractorOutput:
        latex_mathml = {}
        error_expressions = []

        replacing_function=cls.__reg_cleaner(latex_mathml, error_expressions)
        replaced = cls.replace_math_pattern.sub(replacing_function,content)

        tree = BeautifulSoup(replaced)
        cleaned_file_content = u''.join(tree.findAll(text=True))
        # cleaned_file_content = tree.getText()
        return MathExtractorOutput(cleaned_file_content, latex_mathml, error_expressions)

    @classmethod
    def parse_from_xml(cls, filename) -> MathExtractorOutput:
        """


        :param filename:
        :rtype : MathExtractorOutput
        """
        with open(filename, 'r', encoding='utf-8') as myfile:
            content = myfile.read()

        # with open(filename,'r') as myfile:
        # content = myfile.read().replace('\n', ' ')

        # recognize any ht
        content = str(BeautifulSoup(content))

        moutput = cls.input_cleaner(content)
        moutput.math_token_positions = cls.math_token_positions(moutput.cleaned_file_content)

        if len(moutput.unparsed_expressions) != 0:
            for exception in moutput.unparsed_expressions:
                print("Failed to parse expression in " + filename + " :" + exception, file=sys.stderr)

        return moutput



