import csv
import os
from sys import argv
import json
import fnmatch
import sys
from tangent.common.utility.title_getter import GenericTitleGetter, HtmlTitleGetter, MappingTitleGetter

__author__ = 'Nidhin'


def print_help_and_exit():
    """
    Prints usage statement
    """

    usage_string = "Usage: python create_document_ids.py <directory containing files to index> [generic|html|map] output_file \n " \
                   "generic: title is the file name of document \n " \
                   "html: there is a title tag in the thml/xml document \n " \
                   "map: separate file that maps a filename to title: pass as last argument";

    exit(usage_string)


def process_directory(directory, starting_id, output_file, title_getter):
    """
    :type directory: string
    :param directory: directory to start indexing from

    :type starting_id: int
    :param starting_id: id to assign the first document

    :type output_file: string
    :param output_file: id to assign the first document

    """
    doc_id = starting_id
    file_mapping = []

    for dirname, dirnames, filenames in os.walk(directory):
        filenames = [file for file in filenames if file.endswith(('.html', '.xhtml', '.mml'))]

        for filename in filenames:
            full_filename = os.path.join(dirname, filename)
            title = title_getter.get_title(full_filename)
            file_mapping.append((full_filename, doc_id, title))
            doc_id += 1

            if doc_id % 100 == 0:
                print(("Processed %d docs" % (doc_id)))

    with open(output_file, 'w', newline='', encoding='utf-8') as outfile:
        writer = csv.writer(outfile, delimiter='\t', quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)
        writer.writerows(file_mapping)

    print(("Started at number %d" % (starting_id)))
    print(("Found %d files in %s" % (len(file_mapping), directory)))
    print(("Mapping saved at  %s" % (output_file)))


if __name__ == '__main__':


    if len(argv) >= 4:
        directory = argv[1]
        system = argv[2]
        output_file = argv[3]
        title_getter = None

        if system == 'generic':
            title_getter = GenericTitleGetter()
        elif system == 'html':
            title_getter = HtmlTitleGetter()
        elif system == 'map':
            title_getter = MappingTitleGetter(argv[4])
        else:
            print("Unrecognzed option" + system, file=sys.stderr)
            sys.exit()

        process_directory(directory, 0, output_file, title_getter)
    else:
        print_help_and_exit()
else:
    print(argv)
    print_help_and_exit()