import csv
from bs4 import BeautifulSoup


__author__ = 'Nidhin'

from os.path import basename


class GenericTitleGetter:
    def get_title(self, path):
        return basename(path)


class HtmlTitleGetter:
    def get_title(self, path):
        with open(path, 'r', encoding='utf-8') as myfile:
            content = myfile.read()

        # with open(filename,'r') as myfile:
        # content = myfile.read().replace('\n', ' ')

        # recognize any ht
        soup = BeautifulSoup(content)
        title = soup.title.text
        return title


class MappingTitleGetter:
    def __init__(self, mapping_file):
        self.file_mapping_title = {}
        with open(mapping_file, 'r', encoding='utf-8') as myfile:
            reader = csv.reader(myfile)
            for row in reader:
                filename, title = row
                self.file_mapping_title[filename] = title


    def get_title(self, path):
        filename = basename(path)
        #if file not found in document return fiel name
        return self.file_mapping_title.get(filename,filename)



