import codecs
import json
import sys

from stemming.porter2 import stem
from common.search.metadataStore import Metadastore
from common.search.model.Hit import Hit
from common.search.model.MathResult import MathDocumentResult
from common.utility.math_extractor import MathExtractor


__author__ = 'Nidhin'
import math


def calculate_position_score(query_tokens, token_postions):
    assert isinstance(query_tokens, list)
    assert isinstance(token_postions, dict)
    num_tokens = len(query_tokens)

    if num_tokens == 1 and query_tokens[0] in token_postions:
        return 1, "one token"

    if num_tokens == 1 and query_tokens[0] not in token_postions:
        return 0, "no match"

    previous_token = ""
    current_token = ""
    scores = []
    for i in xrange(1, num_tokens):
        previous_token = query_tokens[i - 1]
        current_token = query_tokens[i]

        min_distance = sys.maxint

        #token pairs not found
        if previous_token not in token_postions or current_token not in token_postions:
            scores.append(0)
        else:
            #get minimum distance betwwwen two tokens
            for pt_pos in token_postions[previous_token]:
                for ct_pos in token_postions[current_token]:
                    dis = math.fabs(ct_pos - pt_pos)
                    if dis < min_distance:
                        min_distance = dis

            if min_distance > 30 or min_distance == 0:
                min_distance = 30
            log_distance = (math.log(30) - math.log(min_distance)) / math.log(30)
            scores.append(log_distance)

    pos_reason = "positions:" + json.dumps(token_postions)
    pos_reason = pos_reason + ";scores:" + json.dumps(scores)
    return sum(scores) / (num_tokens - 1), pos_reason


def create_HitResult(cleaned_tokens, doc, math_query, math_results, text_query, text_results):
    h = Hit()
    mds = Metadastore()
    h.token_positions = {}
    if math_query and doc in math_results:
        """:type MathDocumentResult"""
        math_doc = math_results[doc]
        assert isinstance(math_doc, MathDocumentResult)

        h.math_score = math_doc.max_score
        h.math_reason = math_doc.expression.latex
        h.expression_reason=math_doc.debug_info
        h.id = doc
        h.token_positions[math_doc.expression.latex] = math_doc.positions[math_doc.expression.latex]

        #h.token_positions[h.math_reason]=text_results.doc_positions[h.math_reason]
    if text_query and doc in text_results.doc_id_paths.keys():
        h.text_score = text_results.doc_score[doc]

        h.text_reason = ""
        for q in text_query.split():
            token_positions = text_results.doc_positions[doc]
            if q in token_positions:  #else it was possibly a stop word
                h.token_positions[q] = token_positions[q]
                h.text_reason = h.text_reason + " " + q + ":found"  #word found
            else:
                h.text_reason = h.text_reason + " " + q + ":not_found"
    h.fragment = generate_snippet_from_file(mds.get_path_for_id(int(doc)), h.token_positions)
    h.pos_score, h.pos_reason = calculate_position_score(cleaned_tokens, h.token_positions)
    return h


def generate_snippet_from_file(path, term_positions={}):
    content=""
    with codecs.open(path, 'r', "utf-8") as myfile:
            content = myfile.read().replace('\n', ' ')
    content, _, _ = MathExtractor.input_cleaner(content)
    return generate_snippet(content, term_positions)


def generate_snippet(file_content, term_positions={}):
    interested_fragments = []
    split_pattern = u"<math.*?>.*?</math>|\\$+.*?\\$+|[^<\\s]+"
    import re

    split_pattern = re.compile(split_pattern, re.DOTALL)

    tokens = split_pattern.findall(file_content)

    for t in term_positions.keys():
        #TODO fix solr position
        indices = [i for i, x in enumerate(tokens) if stem(x) == t]
        for pos in indices:
        #for pos in term_positions[t]:
            fragment = tokens[pos - 5:pos + 5]
            score = 0

            for word in fragment:
                if stem(word) in term_positions:
                    score += 1
            interested_fragments.append((fragment, score, pos - 5, pos + 5))

    interested_fragments = sorted(interested_fragments, key=lambda elem: elem[1],
                                  reverse=True)

    min_bounday = -1
    max_boundary = -1

    found_fragments = 0

    replace_math_pattern = u"<math.*?>.*?</math>"
    replace_math_pattern = re.compile(replace_math_pattern, re.DOTALL)

    idx = 0
    fragment = ""


    def reg_cleaner_helper(input):
        input = input.group(0)
        if input.startswith(u"<math>") and input.endswith(u"</math>"):
            input = input.replace(u"<math>", "")
            input = input.replace(u"</math>", "")
            input = input.replace(u"</math>", "")
            input = "$" + input + "$"
        return input


    while (found_fragments < 2 and idx < len(interested_fragments)):
        content, score, start, end = interested_fragments[idx]
        if start > max_boundary:  #share indices with previous fragment
            content = " ".join(content)
            content = replace_math_pattern.sub(reg_cleaner_helper, content)
            fragment += "..." + content
            found_fragments = found_fragments + 1
        min_bounday = start
        max_boundary = end
        idx = idx + 1

    return fragment
