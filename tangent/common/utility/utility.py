__author__ = 'ntp5633'


def vbyte_encode(num):
    """
    Given an integer convert it to a variable byte encoding scheme

    @type num:int
    @param num:an ineger to convert to


    @rtype: str
    @return: a byte encoding of the number
    """

    byte_encoding = []

    while (num != 0):
        if (num < 128):
            byte_encoding.append(num)
            break

        byte_encoding.append(num % 128)
        num = num / 128


    # flip the most siginificant bit
    if len(byte_encoding) == 0:
        byte_encoding.append(128)
    else:
        byte_encoding[0] += 128

    #reverse do the last element
    byte_encoding.reverse()
    return str(bytearray(byte_encoding))


def vbyte_decode(byte_enoded_string):
    """
    Given a byte encoded string convert it to an int

    @type byte_enoded_string:str
    @param byte_enoded_string:an ineger to convert to


    @rtype: int
    @return: an integer
    """

    byte_array = bytearray(byte_enoded_string)
    encodednum = 0
    for i in range(len(byte_array) - 1):
        encodednum = 128 * encodednum + byte_array[i]

    encodednum = 128 * encodednum + byte_array[-1] - 128
    return encodednum


def vbyte_encode_list(list_of_num):
    """
    Given a list of numbers, return a gap encoded number

    @type list_of_num: list(int)
    @param list_of_num: a list of numbers

    @rtype: list(str)
    @return: list of gap encoded strings
    """

    if not list_of_num or (len(list_of_num) == 0):
        return
    list_of_num = [int(i) for i in list_of_num]
    list_of_num.sort()
    encoded = []

    # first time
    previous = list_of_num[0]

    encoded.append(vbyte_encode(previous))

    for i in range(1, len(list_of_num)):
        diff = list_of_num[i] - previous
        previous = list_of_num[i]
        encoded.append(vbyte_encode(diff))

    return encoded


def gap_endoded_list(list_of_num):
    if not list_of_num or (len(list_of_num) == 0):
        return
    list_of_num = [int(i) for i in list_of_num]
    list_of_num.sort()
    encoded = []

    # first time
    previous = list_of_num[0]

    encoded.append(previous)

    for i in range(1, len(list_of_num)):
        diff = list_of_num[i] - previous
        previous = list_of_num[i]
        encoded.append(diff)

    return encoded


def run_length_encoded(msg):
    from itertools import groupby

    r = ""
    for i, j in groupby(msg):
        s = sum(1 for _ in j)
        if s > 1:
            r = r + str(s) + str(i)
        else:
            r = r + i
    return r