import codecs
import csv
import json


__author__ = 'Nidhin'


class Metadastore:
    def __init__(self,path_file):
        self.mapping_docid_path = {}
        self.mapping_docid_title={}
        path = path_file
        content = ""

        with open(path,  newline='',encoding='utf-8') as mapping_file:

                reader = csv.reader(mapping_file,  delimiter='\t',quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)

                for row in reader:
                    id=row[1]
                    full_path=row[0]
                    title=row[2]
                    self.mapping_docid_path[id]=full_path
                    self.mapping_docid_title[id]=title




    def get_path_for_id(self, id):
        return self.mapping_docid_path.get(id,"")

    def get_file_name_for_id(self, id):

        p=self.mapping_docid_title[id]
        return p