__author__ = 'Nidhin'


class Hit:
    def __init__(self, docid=0, text_score=0, math_score=0, pos_score=0, fragment="", math_reason="", text_reason="", pos_reason=""):
        self.id = docid
        self.text_score = text_score
        self.math_score = math_score
        self.pos_score = pos_score
        self.fragment = fragment
        self.math_reason = math_reason
        self.text_reason = text_reason
        self.pos_reason = pos_reason
        self.token_positions={}
        self.title=""
        self.full_path=""
        self.final_score=0


