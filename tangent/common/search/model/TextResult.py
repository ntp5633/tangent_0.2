__author__ = 'Nidhin'


class TextDocumentHeader:
    def __init__(self, json_data, query):
        self.maxScore = json_data['response']['maxScore']
        self.numResults = json_data['response']['numFound']
        self.doc_id_paths, self.doc_filepaths, self.doc_titles, self.doc_score = self.parseDocumentInfo(
            json_data['response']['docs'])
        self.query = set(query.split())
        self.doc_positions = self.parsePositionInfo(json_data['termVectors'], query)


    def parseDocumentInfo(self, doc_list):
        doc_scores = {}
        doc_titles = {}
        doc_id_paths = {}
        doc_file_paths = {}

        for d in doc_list:
            doc_file_paths[d['id']] = d['file_location']
            doc_titles[d['id']] = d.get('title',"")
            doc_scores[d['id']] = d['score']
            doc_id_paths[d['id']] = d['file_location']

        return doc_id_paths, doc_file_paths, doc_titles, doc_scores

    def parsePositionInfo(self, termVectorsJson, query_terms):
        doc_id_position_mapping = {}
        for t in termVectorsJson:
            if isinstance(t, list):
                id = t[1]  #document identifier
                token_positon_mappping = {}
                positions = t[3]
                for i in range(0, len(positions), 2):
                    token = positions[i]
                    if token in self.query:# or "<math" in token:  #if token in query or math
                        token_positions = map(int, positions[i + 1][1][1::2])  #odd indices have position
                        token_positon_mappping[token] = token_positions
                        doc_id_position_mapping[id] = token_positon_mappping
        return doc_id_position_mapping
