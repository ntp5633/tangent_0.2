__author__ = 'Nidhin'


class MathDocumentResult:
    def __init__(self, id, num_expressions):
        self.document_id = id
        self.expressions = [None] * num_expressions
        self.positions = {}

    def add_expression(self, exp_num, latex, score, expression_id, matched_pairs,pairs_in_query):
        if self.expressions[exp_num] is None:
            exp = MathExrpessionResult(latex, score, expression_id,matched_pairs,pairs_in_query)
            self.expressions[exp_num] = exp
        else:
            if score > self.expressions[exp_num].score:
                self.expressions[exp_num] = MathExrpessionResult(latex, score, expression_id,matched_pairs,pairs_in_query)


    def calculate_score(self,total_search_pair_count):
        final_score=0
        for exp in self.expressions:
            if exp is not None:
                final_score=final_score+exp.score*len(exp.pairs_in_query)
        return final_score/total_search_pair_count


    def get_math_reason(self):
        l=[]
        for e in self.expressions:
            if e is not None:
                l.append((e.latex,e.score))
        return str(l)

    def get_found_expressions(self):
        l=[]

        for e in self.expressions:
            if e is not None:
                l.append(e.latex)
        return l

class MathExrpessionResult:
    def __init__(self, latex, score, expression_id,matched_pairs,pairs_in_query):
        self.latex = latex
        # self.symbol_pairs = symbol_pairs
        self.score = score
        self.expression_id = expression_id
        self.matched_pairs=matched_pairs
        self.pairs_in_query=pairs_in_query
