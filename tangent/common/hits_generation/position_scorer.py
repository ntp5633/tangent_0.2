import sys
import json
import jsonpickle
import math

__author__ = 'Nidhin'


class NaivePoistionSocrer(object):
    @classmethod
    def calculate_position_score(cls, query_tokens, token_postions):
        assert isinstance(query_tokens, list)
        assert isinstance(token_postions, dict)
        num_tokens = len(query_tokens)

        if num_tokens == 1 and query_tokens[0] in token_postions:
            return 1, "one token"

        if num_tokens == 1 and query_tokens[0] not in token_postions:
            return 0, "no match"

        previous_token = ""
        current_token = ""
        scores = []
        pos_reason=""
        for i in range(1, num_tokens):
            previous_token = query_tokens[i - 1]
            current_token = query_tokens[i]

            min_distance = sys.maxsize
            final_score=0

            # token pairs not found
            if previous_token not in token_postions or current_token not in token_postions:
                final_score=0
            else:
                #get minimum distance betwwwen two tokens
                pt_positions=list(token_postions[previous_token])
                ct_positions=list(token_postions[current_token])
                for pt_pos in pt_positions:
                    for ct_pos in ct_positions:
                        dis = math.fabs(ct_pos - pt_pos)
                        if dis < min_distance:
                            min_distance = dis

                #if min_distance > 30 or min_distance == 0:
                #    min_distance = 30
                #log_distance = (math.log(30) - math.log(min_distance)) / math.log(30)

                final_score=0
                if min_distance>10:
                    final_score=0
                else:
                    final_score = (1 - ((min_distance-1)/10))

            scores.append(final_score)
            pos_reason=pos_reason + " ("+previous_token+","+current_token+"):"+str(min_distance)+";"

        #pos_reason = "positions:" + jsonpickle.encode(token_postions)
        pos_reason = pos_reason + "scores:" + json.dumps(scores)
        return sum(scores) / (num_tokens - 1), pos_reason
