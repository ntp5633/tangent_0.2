import re
import string
import sys
from tangent.common.hits_generation.fragment_generator import BaseFragmentGenerator

from tangent.common.hits_generation.hit_generator import HitGenerator
from tangent.common.utility.math_extractor import MathExtractor


__author__ = 'Nidhin'

import networkx as nx

from nltk.stem.porter import PorterStemmer


class SentenceGraph:
    __slots__ = ['word_node_dict', 'graph', 'shortest_paths', 'sentence_id_to_terms', 'sentence_id_original_senences']

    def __init__(self, sentences=[]):
        G = nx.Graph()
        self.word_node_dict = {}
        # add sentence nodes
        cleaned_sentences = []
        self.sentence_id_to_terms = {}
        self.sentence_id_original_senences = {}
        for idx, sentence in enumerate(sentences):
            cleaned_sentence = self.tokenize_and_stem(sentence)
            cleaned_sentence_dict = {}
            for t in cleaned_sentence:
                cleaned_sentence_dict[t] = cleaned_sentence_dict.get(t, 0)
                cleaned_sentence_dict[t] = cleaned_sentence_dict[t] + 1

            sentence_id = idx
            sentence_id=str(sentence_id)
            self.sentence_id_to_terms[sentence_id] = cleaned_sentence_dict
            self.sentence_id_original_senences[sentence_id] = sentence

            G.add_node(sentence_id)
            for term in cleaned_sentence_dict.keys():
                # add terrm->sentence
                self.word_node_dict[term] = self.word_node_dict.get(term, set())
                self.word_node_dict[term].add(sentence_id)
            cleaned_sentences.append(cleaned_sentence_dict)

        self.__create_sentence_weights(cleaned_sentences, G)

        self.graph = G
        self.shortest_paths = nx.all_pairs_shortest_path(G)

    @classmethod
    def tokenize_and_stem(self, text):
        regex = re.compile('[%s]' % re.escape(string.punctuation))

        tokens = re.findall("<math.*?>.*?</math>|\\$+.{1,200}?\\$+|[^<\\s]+",text)
        ps = PorterStemmer()
        stemmed_list = []
        for item in tokens:
            stemmed_token = item
            if not (item.startswith(u"<math>") and item.endswith(u"</math>")):
                stemmed_token = regex.sub("", stemmed_token)
                stemmed_token = ps.stem(stemmed_token.lower())
            stemmed_list.append(stemmed_token)
        return stemmed_list


    @classmethod
    def sentence_similarity(cls, sentence_1, sentence_2):
        if (len(sentence_1.keys()) == 0 or len(sentence_2.keys()) == 0):
            return 0
        assert isinstance(sentence_1, dict)
        assert isinstance(sentence_2, dict)

        shared_terms = set(sentence_1.keys()) & set(sentence_2.keys())
        size_sent_1 = sum(sentence_1.values())
        size_sent_2 = sum(sentence_2.values())

        text_similarity = 0
        for t in shared_terms:
            text_similarity = sentence_1[t] + sentence_2[t]

        score = text_similarity / (size_sent_1 + size_sent_2)
        return score


    def __create_sentence_weights(self, sentences, G):
        assert isinstance(G, nx.Graph)

        avg_sentence_score = 0
        # similairty score between all frgaments
        for i in range(0, len(sentences)):
            sentence_i = sentences[i]
            for j in range(i + 1, len(sentences)):
                sentence_j = sentences[j]
                score = SentenceGraph.sentence_similarity(sentence_i, sentence_j)
                avg_sentence_score = avg_sentence_score + score
                G.add_edge(i, j, weight=score)

        avg_sentence_score = avg_sentence_score / len(sentences)

        min_threshold_score = 0.30 * avg_sentence_score

        # remove nodes belwo threshold
        for (n1, n2, attributes) in G.edges(data=True):
            if attributes['weight'] < min_threshold_score:
                G.remove_edge(n1, n2)
                # trim nodes that don't meet threshold score


class SentenceGraphFragmentGenerator(BaseFragmentGenerator):
    @classmethod
    def generate_sentence_graph(cls, path) -> SentenceGraph:
        content = MathExtractor.parse_from_xml(path).cleaned_file_content

        sentences=re.split(r"\\n(?!</math>)",content)

        filtered_sentences=[]

        for s in sentences:
            trimmed=s.strip()
            filtered_sentences.append(trimmed)


        graph = SentenceGraph(filtered_sentences)
        return graph

    @classmethod
    def generate_snippet_from_file(cls,path, term_positions={},hit_specific_list=None):

        graph=cls.generate_sentence_graph(path)


        return cls.generate_snippet(hit_specific_list, graph)


    @classmethod
    def generate_snippet(cls, terms_in_query, sentence_graph) -> string:


        assert isinstance(sentence_graph, SentenceGraph)
        assert isinstance(terms_in_query, list)
        graph = sentence_graph.graph
        assert isinstance(graph, nx.Graph)

        terms_in_query_set = {}
        uniq_terms_in_query = []
        for t in terms_in_query:
            terms_in_query_set[t] = terms_in_query_set.get(t, 0)
            terms_in_query_set[t] = terms_in_query_set[t] + 1
            if t not in uniq_terms_in_query:
                uniq_terms_in_query.append(t)
        node_score = {}
        sentence_terms_related_to_query = {}
        for (node_id, cleaned_sentence) in sentence_graph.sentence_id_to_terms.items():
            s1 = cleaned_sentence
            assert isinstance(s1, dict)
            score = SentenceGraph.sentence_similarity(s1, terms_in_query_set)
            node_score[node_id] = score
            matched_terms_in_query = set(s1.keys()).intersection(terms_in_query_set.keys())
            sentence_terms_related_to_query[node_id] = matched_terms_in_query



        # terms avaialble in document realted to query
        terms_in_graph = set(terms_in_query_set.keys()) & set(sentence_graph.word_node_dict.keys())

        uniq_terms_in_query_in_doc = []

        for t in uniq_terms_in_query:
            if t in terms_in_graph:
                uniq_terms_in_query_in_doc.append(t)

        if len(terms_in_graph) == 0:
            return ""
        all_paths = []

        visited_node_neighborhoods = set()

        for t in uniq_terms_in_query_in_doc:
            nodes_containing_term = sentence_graph.word_node_dict[t]
            nodes_containing_term_sorted = {}

            # sort nodes by similiarity to query
            for n in nodes_containing_term:
                nodes_containing_term_sorted[str(n)] = node_score[str(n)]

            nodes_containing_term_sorted = sorted(nodes_containing_term_sorted.keys(),
                                                  key=lambda k: nodes_containing_term_sorted[k], reverse=True)

            for n in nodes_containing_term_sorted:

                terms_in_sentence = sentence_terms_related_to_query[n]
                terms_to_look_for = terms_in_graph - terms_in_sentence
                nodes_visited = set()
                nodes_visited.add(n)

                p = Path(current_node=n,
                         previous_path=None, terms_to_find=terms_to_look_for, term_found=terms_in_sentence,
                         node_dict=sentence_graph.word_node_dict,
                         all_pairs_shortest_path=sentence_graph.shortest_paths,
                         nodes_visited=nodes_visited, node_to_expand_to=None,
                         sentence_terms_related_to_query=sentence_terms_related_to_query)

                neighborhood = frozenset(p.nodes_in_path)
                if neighborhood not in visited_node_neighborhoods:
                    visited_node_neighborhoods.add(neighborhood)
                    all_paths.append(p)

        snippet_found = False;

        best_path = None
        best_path_score = sys.maxsize
        best_path_all_terms_found = False

        while not snippet_found:
            new_paths = []
            for path in all_paths:
                if path.done() or path.is_dead_path():
                    score = cls.__score_path(path, sentence_graph, node_score)

                    if not len(path.terms_to_find) == 0:
                        if score < best_path_score:
                            best_path = path
                            best_path_score = score
                    else:
                        if score < best_path_score:
                            best_path = path
                            best_path_score = score
                            best_path_all_terms_found = True


                else:
                    new_sub_paths = path.make_next_move()
                    if new_sub_paths:
                        for p in new_sub_paths:
                            if p.is_dead_path():
                                new_paths.append(p)

                            elif p.node_to_expand_to:  # if we are expanding a  node to another
                                new_paths.append(p)

                            else:
                                neighborhood = frozenset(p.nodes_in_path)
                                if neighborhood not in visited_node_neighborhoods:
                                    visited_node_neighborhoods.add(neighborhood)
                                    new_paths.append(p)

            all_paths = new_paths
            if best_path_all_terms_found:
                snippet_found = True

            if len(all_paths) == 0:
                snippet_found = True

        return cls.__get_summary_from_path(best_path, sentence_graph)

    @classmethod
    def __get_summary_from_path(cls, path, sentence_graph) -> string:
        if path is None:
            return ""
        assert isinstance(path, Path)
        assert isinstance(sentence_graph, SentenceGraph)

        nodes_in_path = []
        nodes_seen = set()
        while path != None:
            node_id = path.current_node

            sentence = sentence_graph.sentence_id_original_senences[node_id]


            nodes_in_path.append((node_id, sentence))
            nodes_seen.add(node_id)

            path = path.previous_path

        if (len(nodes_in_path)) < 3:
            for k in sentence_graph.sentence_id_original_senences.keys():
                k=str(k)
                if k not in nodes_in_path and len(nodes_in_path) < 3:
                    s=sentence_graph.sentence_id_original_senences[k]
                    if len(s)!=0:
                        nodes_in_path.append((k, s))
                if  len(nodes_in_path) >= 3:
                    break

        nodes_in_path_trimmed = nodes_in_path[:3]
        nodes_in_path_trimmed.sort()

        summary = ""

        for (n, s) in nodes_in_path_trimmed:
            summary = summary + " " + s
        return summary

    @classmethod
    def __score_path(cls, path, sentence_graph, node_score) -> float:
        score = 0.0
        node_scores = 0.0
        edge_scores = 0.0

        while path is not None:
            node_id = path.current_node
            n_score = node_score[node_id]
            e_score = 0
            if path.previous_path:
                previous_id = path.previous_path.current_node
                e_score = sentence_graph.graph[node_id][previous_id]['weight']

            node_scores += n_score
            edge_scores += e_score

            path = path.previous_path

        if edge_scores > 0:
            score = score + 1 * (1.0 / edge_scores)
        if node_scores > 0:
            score = score + 0.5 * (1.0 / node_scores)

        return score


class Path:
    def __init__(self, current_node, previous_path, terms_to_find, node_dict, all_pairs_shortest_path, nodes_visited,
                 node_to_expand_to, term_found, sentence_terms_related_to_query):
        assert isinstance(current_node, str)
        assert isinstance(terms_to_find, set)
        assert isinstance(node_dict, dict)

        self.current_node = current_node
        self.previous_path = previous_path
        self.terms_to_find = set()
        self.terms_to_find = self.terms_to_find.union(terms_to_find)
        self.node_dict = node_dict

        self.node_to_expand_to = node_to_expand_to
        self.nodes_in_path = set()
        self.nodes_in_path = self.nodes_in_path.union(nodes_visited)
        self.all_pairs_shortest_path = all_pairs_shortest_path

        self.terms_found = set()
        self.terms_found = self.terms_found.union(term_found)

        self.dead_path = False
        if node_to_expand_to is not None:

            self.shortest_path = all_pairs_shortest_path[current_node].get(node_to_expand_to)
            if self.shortest_path is None:
                self.node_to_expand_to = None
                self.dead_path = True

        self.sentence_terms_related_to_query = sentence_terms_related_to_query


    def make_next_move(self):
        paths_to_explore = []
        if self.done() or self.is_dead_path():
            return

        if self.node_to_expand_to is not None:  # if there is a node to expand to
            # expanding a node
            new_node = self.shortest_path[1]
            new_node=str(new_node)
            new_node_terms = self.sentence_terms_related_to_query[new_node]

            terms_found = self.terms_found.union(new_node_terms)
            terms_to_find = self.terms_to_find - terms_found
            nodes_in_path = self.nodes_in_path.union(set())
            nodes_in_path.add(new_node)

            node_to_expand_to = self.node_to_expand_to

            if node_to_expand_to == new_node:
                node_to_expand_to = None

            graph_t = Path(current_node=new_node,
                           previous_path=self, terms_to_find=terms_to_find,
                           term_found=terms_found,
                           node_dict=self.node_dict,
                           all_pairs_shortest_path=self.all_pairs_shortest_path,
                           nodes_visited=nodes_in_path, node_to_expand_to=node_to_expand_to,
                           sentence_terms_related_to_query=self.sentence_terms_related_to_query)

            paths_to_explore.append(graph_t)

        else:
            for t in self.terms_to_find:
                for node in self.node_dict[t]:
                    node =str(node)
                    shared_terms = self.sentence_terms_related_to_query[node].intersection(self.terms_found)
                    # if len(shared_terms!=0):
                    if node not in self.nodes_in_path:
                        graph_t = Path(current_node=self.current_node,
                                       previous_path=self.previous_path, terms_to_find=self.terms_to_find,
                                       term_found=self.terms_found,
                                       node_dict=self.node_dict,
                                       all_pairs_shortest_path=self.all_pairs_shortest_path,
                                       nodes_visited=self.nodes_in_path, node_to_expand_to=node,
                                       sentence_terms_related_to_query=self.sentence_terms_related_to_query)
                        graph_t.shortest_path_ix = 1

                        paths_to_explore.append(graph_t)
                        # for the last path, update currrent

        return paths_to_explore

    def done(self):
        if len(self.terms_to_find) == 0:
            return True
        return False


    def is_dead_path(self):
        return self.dead_path


