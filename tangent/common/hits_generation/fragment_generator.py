__author__ = 'Nidhin'


from tangent.common.hits_generation.hit_generator import HitGenerator
from tangent.common.utility.math_extractor import MathExtractor



class BaseFragmentGenerator:
    @classmethod
    def generate_snippet_from_file(cls, path, term_positions={},hit_specific_list=None):
        return ""

    @classmethod
    def generate_snippet(cls,file_content, term_positions={},hit_specific_list=None):
        return ""


class FullFileFragmentGenerator(BaseFragmentGenerator):
    @classmethod
    def generate_snippet_from_file(cls, path, term_positions={},hit_specific_list=None):
        file_content=None
        with open(path, "r", encoding='utf-8') as myfile:
            file_content = myfile.read().replace("\n", "")
        return cls.generate_snippet(file_content)

    @classmethod
    def generate_snippet(cls,file_content, term_positions={},hit_specific_list=None):
        moutput = MathExtractor.input_cleaner(file_content)
        out = moutput.cleaned_file_content
        return out

