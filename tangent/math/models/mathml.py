__author__ = 'Nidhin'


class MathML:
    """
    List of recognized tags
    """
    math = '{http://www.w3.org/1998/Math/MathML}math'
    mn = '{http://www.w3.org/1998/Math/MathML}mn'
    mo = '{http://www.w3.org/1998/Math/MathML}mo'
    mi = '{http://www.w3.org/1998/Math/MathML}mi'
    mtext = '{http://www.w3.org/1998/Math/MathML}mtext'
    mrow = '{http://www.w3.org/1998/Math/MathML}mrow'
    msub = '{http://www.w3.org/1998/Math/MathML}msub'
    msup = '{http://www.w3.org/1998/Math/MathML}msup'
    msubsup = '{http://www.w3.org/1998/Math/MathML}msubsup'
    munderover = '{http://www.w3.org/1998/Math/MathML}munderover'
    msqrt = '{http://www.w3.org/1998/Math/MathML}msqrt'
    mroot = '{http://www.w3.org/1998/Math/MathML}mroot'
    mfrac = '{http://www.w3.org/1998/Math/MathML}mfrac'
    mfenced = '{http://www.w3.org/1998/Math/MathML}mfenced'
    mover = '{http://www.w3.org/1998/Math/MathML}mover'
    munder = '{http://www.w3.org/1998/Math/MathML}munder'
    mpadded = '{http://www.w3.org/1998/Math/MathML}mpadded'
    none = '{http://www.w3.org/1998/Math/MathML}none'
    mstyle = '{http://www.w3.org/1998/Math/MathML}mstyle'
    mspace = '{http://www.w3.org/1998/Math/MathML}mspace'
    mtable = '{http://www.w3.org/1998/Math/MathML}mtable'
    mtr = '{http://www.w3.org/1998/Math/MathML}mtr'
    mtd = '{http://www.w3.org/1998/Math/MathML}mtd'
    semantics = '{http://www.w3.org/1998/Math/MathML}semantics'
    mmultiscripts ='{http://www.w3.org/1998/Math/MathML}mmultiscripts'
    mprescripts='{http://www.w3.org/1998/Math/MathML}mprescripts'
    mqvar='{http://search.mathweb.org/ns}qvar'