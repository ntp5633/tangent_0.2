from collections import deque
from tangent.math.models.exceptions import UnknownTagException
from tangent.math.models.mathml import MathML

__author__ = 'Nidhin'


class MathSymbol:
    """
    Symbol in a symbol tree
    """

    def __init__(self, tag, next_elem=None, above=None, below=None, within=None, matrix=False, enclosed=None,
                 matrix_element=None,qvar=False):
        self.tag = tag
        self.next = next_elem
        self.above = above
        self.below = below
        self.within = within
        self.id = None
        self.matrix = matrix
        self.enclosed = enclosed
        self.matrix_element = matrix_element
        self.qvar=qvar

    def build_repr(self, builder):
        """
        Build representation of symbol
        """
        builder.append('(')
        builder.append(self.tag)
        if self.next:
            builder.append(',next=')
            self.next.build_repr(builder)
        if self.above:
            builder.append(',above=')
            self.above.build_repr(builder)
        if self.below:
            builder.append(',below=')
            self.below.build_repr(builder)
        if self.within:
            builder.append(',within=')
            self.within.build_repr(builder)
        if self.enclosed:
            builder.append(',enclosed=')
            self.enclosed.build_repr(builder)
        builder.append(')')


    def build_repr_matrix_element(self, builder):
        """
        Build representation of symbol
        """
        builder.append(self.tag)
        if self.next:
            self.next.build_repr_matrix_element(builder)
        if self.above:
            self.above.build_repr_matrix_element(builder)
        if self.below:
            self.below.build_repr_matrix_element(builder)
        if self.within:
            self.within.build_repr_matrix_element(builder)
        if self.enclosed:
            self.enclosed.build_repr_matrix_element(builder)


    def get_symbols(self):
        return MathSymbolIterator(self)

    def generate_ids(self, prefix=(0,)):
        self.id = prefix
        for child, v_dist in [(self.above, 1), (self.next, 0), (self.below, 2), (self.within, 3)]:
            if child:
                child.generate_ids(prefix + (v_dist,))

    def get_pairs(self):
        """
        Return the pairs in the symbol tree

        :rtype list
        :return list of symbols
        """


        def mk_helper(v_dist_diff):
            def helper(tup):
                right, h_dist, v_dist = tup
                return (self.tag, right.tag, h_dist + 1, v_dist + v_dist_diff, right.id)

            return helper

        ret = []
        for child, v_dist in [(self.above, 1), (self.next, 0), (self.below, -1), (self.within, 0)]:
            if child:
                ret.extend(map(mk_helper(v_dist), child.get_symbols()))
                ret.extend(child.get_pairs())

        if self.enclosed:
            ret.extend(self.getMatrixPairs())

        if (len(ret) == 0):  # if no pairs, that means it is an isolated symbol, so return distance from None
            ret.append((self.tag, "None", 0, 0, self.id))
        return ret


    def getMatrixPairs(self):
        dims = self.tag.split("_")
        matrix_row = int(dims[1])
        matrix_cols = int(dims[2])
        pairs = []

        pairs.append(("mstart", "mend", str(matrix_row), str(matrix_cols), [0]))

        matrix_elems = self.enclosed
        assert (matrix_elems, MathSymbol)
        elem = matrix_elems.next  # first element in matrix
        assert (matrix_elems, elem)

        inside_pairs = []
        for i in range(0, matrix_row):
            for j in range(0, matrix_cols):
                elem_content = elem.matrix_element
                elem = elem.next
                builder = []
                if elem_content is not None:
                    elem_content.build_repr_matrix_element(builder)
                    key = "".join(builder)
                    if len(key)>50:
                        key=key[:50]
                    pairs.append(("matrix", key, str(i), str(j), [0]))

                    for s1, s2, dh, dv, path in elem_content.get_pairs():
                        inside_pairs.append(
                            (s1, s2, dh, dv, [0]))

        pairs.extend(inside_pairs)

        return pairs


    @classmethod
    def parse_from_mathml(cls, elem):


        """
        Parse symbol tree from mathml
        """
        if elem.tag == MathML.math:
            children = list(elem)
            if len(children) == 1:
                return cls.parse_from_mathml(children[0])
            elif len(children) == 0:
                return None
            else:
                raise Exception('math element with more than 1 child')
        if elem.tag == MathML.semantics:
            children = list(elem)
            if len(children) >= 1:
                return cls.parse_from_mathml(children[0])
            elif len(children) == 0:
                return None
        if elem.tag == MathML.mstyle:
            children = list(elem)
            if len(children) >= 1:
                return cls.parse_from_mathml(children[0])
            elif len(children) == 0:
                return None
        elif elem.tag == MathML.mrow:
            children_map = filter(lambda x: x is not None and x.tag != u'\u2062', list(map(cls.parse_from_mathml, elem)))
            children = list(children_map)
            cleaned_row = []

            # special processing if ma
            start = 0
            while (start < len(children)):
                elem = children[start]

                if (elem.matrix == True and start > 0 and children[start - 1].tag == '('):
                    cleaned_row.pop()
                    cleaned_row.append(elem)
                    start = start + 2
                else:
                    cleaned_row.append(elem)
                    start = start + 1

            # finish removing matrix paren
            for i in range(1, len(cleaned_row)):
                elem = cleaned_row[i - 1]
                while elem.next:
                    elem = elem.next
                elem.next = cleaned_row[i]

            if (len(cleaned_row)==0):
                return cls('empty_row')
            else:
                return cleaned_row[0]
        elif elem.tag == MathML.mn:
            return cls(elem.text.strip() if elem.text else '')
        elif elem.tag == MathML.mo:
            return cls(elem.text.strip() if elem.text else '')
        elif elem.tag == MathML.mi:

            return cls(elem.text if elem.text else 'EMPTY')
        elif elem.tag == MathML.mtext:
            return cls(elem.text.strip() if elem.text else '')
        elif elem.tag == MathML.mspace:
            return cls(' ')
        elif elem.tag == MathML.msub:
            children = list(map(cls.parse_from_mathml, elem))
            children[0].below = children[1]
            return children[0]
        elif elem.tag == MathML.msup:
            children = list(map(cls.parse_from_mathml, elem))
            children[0].above = children[1]
            return children[0]
        elif elem.tag == MathML.msubsup:
            children = list(map(cls.parse_from_mathml, elem))
            children[0].above = children[1]
            children[0].below = children[2]
            return children[0]
        elif elem.tag == MathML.munderover:
            children = list(map(cls.parse_from_mathml, elem))
            children[0].above = children[1]
            children[0].below = children[2]
            return children[0]
        elif elem.tag == MathML.mover:
            children = list(map(cls.parse_from_mathml, elem))
            children[0].above = children[1]
            return children[0]
        elif elem.tag == MathML.munder:
            children = list(map(cls.parse_from_mathml, elem))
            children[0].below = children[1]
            return children[0]
        elif elem.tag == MathML.msqrt:
            children = list(map(cls.parse_from_mathml, elem))
            if len(children) == 1:
                root = cls('root2')
                root.within = children[0]
                return root
            else:
                raise Exception('msqrt element with != 1 children')
        elif elem.tag == MathML.mroot:
            children = list(map(cls.parse_from_mathml, elem))
            if len(children) == 2:
                root = cls('root' + children[1].tag)
                root.within = children[0]
                return root
            else:
                raise Exception('mroot element with != 2 children')
        elif elem.tag == MathML.mfrac:
            children = list(map(cls.parse_from_mathml, elem))
            if len(children) == 2:
                s = cls('frac')
                s.above = children[0]
                s.below = children[1]
                return s
            else:
                raise Exception('frac element with != 2 children')
        elif elem.tag == MathML.mfenced:
            opening = cls(elem.attrib.get('open', '('))
            closing = cls(elem.attrib.get('close', '('))
            children = list(map(cls.parse_from_mathml, elem))
            separators = elem.attrib.get('separators', ',').split()

            row = [opening]
            if children:
                row.append(children[0])
            for i, child in enumerate(children[1:]):
                row.append(cls(separators[min(i, len(separators) - 1)]))
                row.append(child)
            row.append(closing)

            for i in range(1, len(row)):
                elem = row[i - 1]
                while elem.next:
                    elem = elem.next
                elem.next = row[i]
            return row[0]
        elif elem.tag == MathML.mpadded:
            children = list(filter(lambda x: x is not None and x.tag != u'\u2062', list(map(cls.parse_from_mathml, elem))))
            for i in range(1, len(children)):
                elem = children[i - 1]
                while elem.next:
                    elem = elem.next
                elem.next = children[i]
            return children[0]
        elif elem.tag == MathML.none:
            return None

        elif elem.tag == MathML.mtr:
            children = list(map(cls.parse_from_mathml, elem))
            return children
        elif elem.tag == MathML.mtd:
            children = list(map(cls.parse_from_mathml, elem))
            if (len (children)==0):
                children=[cls('empty_matrix_element')]
            return children
        elif elem.tag == MathML.mtable:
            elements = list(map(cls.parse_from_mathml, elem))
            num_rows = len(elements)
            num_cols = len(elements[0])
            root = cls('matrix_' + str(num_rows) + "_" + str(num_cols), matrix=True)


            # root.enclosed= matrix elements
            main_elems = cls('matrix_elements')
            root.enclosed = main_elems
            for i in range(0, num_rows):
                row_elements = elements[i]
                for j in range(0, num_cols):
                    new_elemenet = cls('matrix_element')
                    new_elemenet.matrix_element = row_elements[j][0]
                    main_elems.next = new_elemenet
                    main_elems = main_elems.next;

            return root

        elif elem.tag == MathML.mprescripts:
            return "PreScript"
        elif elem.tag == MathML.mmultiscripts:
            elements = list(map(cls.parse_from_mathml, elem))

            main = elements[0]
            assert isinstance(main, MathSymbol)

            next=1
            #prescripts
            if elements[next]!='PreScript' and elements[next+1]!='PreScript':
                if elements[next] is not None:
                    main.below = elements[next]
                if elements[next+1] is not None:
                    main.above = elements[next+1]
                next=4
            else:
                next=2

            pre_bottom = None
            pre_top = None
            #post scripts
            if len(elements) >=next:


                if elements[next] is not None:
                    elements[next].above = main
                    pre_bottom = elements[next]
                if elements[next+1] is not None:
                    elements[next+1].below = main
                    pre_top = elements[next+1]

            root = cls('multiscript')
            if pre_top is not None:
                root.above = pre_top

            if pre_bottom is not None:
                root.below = pre_bottom

            if pre_top is None and pre_bottom is None:
                root.next=main
            return root


        elif elem.tag ==MathML.mqvar:
            val=elem.attrib['name']
            root=cls('?'+val,qvar=True)
            return root

        else:
            raise UnknownTagException(elem.tag)


class MathSymbolIterator(object):
    """
    Iterator over a symbol tree
    """

    def __init__(self, node):
        self.stack = deque([(node, 0, 0)] if node else [])

    def __iter__(self):
        return self

    def __next__(self):
        if len(self.stack) < 1:
            raise StopIteration
        (elem, h_dist, v_dist) = self.stack.pop()
        if elem.below:
            self.stack.append((elem.below, h_dist + 1, v_dist - 1))
        if elem.next:
            self.stack.append((elem.next, h_dist + 1, v_dist))
        if elem.above:
            self.stack.append((elem.above, h_dist + 1, v_dist + 1))
        if elem.within:
            self.stack.append((elem.within, h_dist + 1, v_dist))
        return (elem, h_dist, v_dist)