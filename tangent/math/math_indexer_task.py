from tangent.common.search.model.Stats import Stats
from tangent.math.index.mysql_index import MySQLIndex
from tangent.math.models.symboltree import SymbolTree

__author__ = 'Nidhin'


class MathIndexer(object):
    def __init__(self, math_index, doc_mapping):
        self.index(math_index, doc_mapping)

    def index(self, math_index, doc_mapping):
        """
        Index a directory containing .text,'.xhtml', '.mathml', '.mml' files

        """
        assert isinstance(math_index, MySQLIndex)
        block_size = 100

        combined_stats = Stats()

        #for i in range(0, len(doc_mapping)):  # break into chunks of blocksize
        #    tree=doc_mapping[i]
        #    tree=SymbolTree.parse()
        #    sub_doc_list = doc_mapping[i:i + block_size]
        #
        #    math_index.add_all(trees)

        trees = SymbolTree.parse_directory(doc_mapping, combined_stats)
        math_index.add_all(trees)

        math_index.commit_all()

        #print('')
        #print('Added %d expressions from %d documents' % (combined_stats.num_expressions,
                                                          #combined_stats.num_documents))
        #print('Missing tags:')
        #for tag, items in combined_stats.missing_tags.items():
        #    print('    %s (%s)' % (tag, str(items)))

        #print('Problem Files:')
        #for (key, value) in combined_stats.problem_files.items():
        #    print(key + ":" + ', '.join(value))