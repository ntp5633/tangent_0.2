from collections import defaultdict
import sys
import zlib

import simplejson

from tangent.math.index.ExpressionRanker import ExpressionRanker
from tangent.math.index.baseindex import BaseIndex, Result


MAX_UNCOMMITED = 1000
max_results = 1000
max_size_pairs=10000

__author__ = 'Nidhin'

import pymysql as mdb
# import mysql.connector as mdb


class MySQLIndexNtcir(BaseIndex):
    def __init__(self, db, ranker=None, port=3306, host='127.0.0.1', process_id=-1):
        self.symboltree = {}
        self.symbol_mapping = {}
        conn = mdb.connect(host=host, port=port, user='math_searcher', passwd='KEfks5sxIYAA7z7QnNkg', db=db,
                           use_unicode=True,
                           charset='utf8mb4')

        self.ranker = ranker
        self.conn = conn
        self.conn.autocommit(False)
        self.db=db

        # self.conn.autocommit = False
        self.cursor = conn.cursor()

        self._expressions_to_commit = []
        self._symbol_id_map = {}
        self._pairs_to_commit = {}
        self.partial_pairs_to_commit=[]
        self._positions_to_commit = []
        self.symbols_to_commit = []

        self.cursor.execute("SET NAMES 'utf8mb4' COLLATE 'utf8mb4_bin';  ")
        #self.cursor.execute("SET CHARACTER SET utf8mb4; ")
        # self.cursor.execute("SET SQL_LOG_BIN=0 ")

        # self.cursor.execute("SET tx_isolation = 'READ-COMMITTED';")
        self.process_id = process_id

    def flush(self):
        self.cursor.execute("TRUNCATE  table expression")
        self.cursor.execute("TRUNCATE  table expression_docs")
        self.cursor.execute("TRUNCATE  table pairs")
        self.cursor.execute("TRUNCATE  table symbols")
        self.conn.commit()

    def add(self, expression_objects):
        """
        Add symbol tree to index

        :type tree: SymbolTree
        :param tree:Symbol Tree


        """
        """
        Add symbol tree to index

        :type tree: SymbolTree
        :param tree:Symbol Tree


        """

        for e in expression_objects:
            # assert isinstance(e, ExpressionToInsert)

            expr_id = None
            if e.mathml not in self._symbol_id_map:
                expr_id = len(self._symbol_id_map) + 1

                self._symbol_id_map[e.mathml] = expr_id
                self._expressions_to_commit.append((expr_id, e.latex, e.mathml, e.num_pairs))

                for (pair, count) in e.all_pairs:
                    lp, rp, hd, vd = pair.split('|')
                    lid = self.get_symbol_id(lp)
                    rid = self.get_symbol_id(rp)

                    key=(lid, rid, hd, vd)
                    if key not in self._pairs_to_commit:
                        self._pairs_to_commit[key] = []
                    self._pairs_to_commit[key].append((expr_id, count))

                    #if pair has max expression
                    if len(self._pairs_to_commit[key])>max_size_pairs:

                        expressions=self._pairs_to_commit[key]
                        pair=key

                        enty=self.__process_all_pairs_for_insertion(expressions,pair)
                        self.partial_pairs_to_commit.append(enty)
                        del self._pairs_to_commit[key]


            else:
                expr_id = self._symbol_id_map[e.mathml]

            doc_id, pos_string = e.doc_position
            self._positions_to_commit.append((expr_id, doc_id, pos_string))

    def get_symbol_id(self, symbol):
        if symbol in self.symbol_mapping:
            return self.symbol_mapping[symbol]
        else:
            symb_id = len(self.symbol_mapping) + 1
            self.symbol_mapping[symbol] = symb_id
            return symb_id


    def insert_symbols(self):

        symbols = list(self.symbol_mapping.items())
        #self.cursor.execute("SET CHARACTER SET utf8mb4; ")
        self.cursor.executemany("""INSERT   INTO symbols (symbol,id) VALUES(%s,%s) """,
                                (symbols))
        # self.conn.commit()


    def __insert_expressions_docs(self):
        try:
            # self.cursor.executemany("insert into expression_docs(expression_id,doc_id,positions) values(%s,%s,%s)" +
            # "ON DUPLICATE KEY UPDATE positions=positions", self._positions_to_commit)
            self.cursor.executemany("insert  expression_docs(expression_id,doc_id,positions) values(%s,%s,%s)"
                                    , self._positions_to_commit)

            # self.conn.commit()


        except Exception as err:
            print("Failed to add " + str(len(self._positions_to_commit)) + " expression_docs", file=sys.stderr)
            print("Failed to add doc such as:" + str(self._positions_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err



    def __process_all_pairs_for_insertion(self, expressions, pair):
            lpair, rpair, hdistance, vdistance = pair
            str_value = simplejson.dumps(expressions)
            compressed_str_value=zlib.compress(str_value.encode('utf-8'),9)
            entry = (lpair, rpair, hdistance, vdistance, compressed_str_value, len(expressions))
            return entry

    def __insert_pairs(self):


        def chunks(l, n):
            """ Yield successive n-sized chunks from l.
            """

            for i in range(0, len(l), n):
                tmp = []
                for pair, expressions in l[i:i + n]:
                    entry = self.__process_all_pairs_for_insertion(expressions, pair)
                    tmp.append(entry)
                yield tmp


        try:

            self.cursor.execute("Alter table pairs DISABLE KEYS")

            inserted=0
            total_pairs=len(self._pairs_to_commit)
            for chunk in chunks(list(self._pairs_to_commit.items()), MAX_UNCOMMITED):
                self.cursor.executemany(
                    "insert  into pairs(lpair,rpair,hdistance,vdistance,expressions,num_expressions) values(%s,%s,%s,%s,%s,%s)"
                    ,
                    chunk)
                inserted+=len(chunk)
                print("Inserted %s of %s" %(inserted,total_pairs ))
            self.cursor.execute("Alter table pairs ENABLE KEYS")
            # self.conn.commit()

            return True
        except Exception as err:
            print("Failed to add " + str(len(self._pairs_to_commit)) + " pairs", file=sys.stderr)
            sample_pair=list(self._pairs_to_commit.keys())[0]
            print("Failed to add pairs such as:" + str(sample_pair), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err









    def __insert_long_pairs(self):

        try:

            self.cursor.execute("Alter table pairs DISABLE KEYS")
            self.cursor.executemany(
                    "insert  into pairs(lpair,rpair,hdistance,vdistance,expressions,num_expressions) values(%s,%s,%s,%s,%s,%s)"
                    ,
                    self.partial_pairs_to_commit)
            #self.cursor.execute("Alter table pairs ENABLE KEYS")
            # self.conn.commit()

            return True
        except Exception as err:
            print("Failed to add " + str(len(self.partial_pairs_to_commit)) + " pairs", file=sys.stderr)
            print("Failed to add pairs such as:" + str(self.partial_pairs_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err


    def __insert_expressions(self):
        try:
            self.cursor.executemany(
                """insert  into expression(id,latex,mathml,fmeasure_score) values(%s,%s,%s,%s) """,
                self._expressions_to_commit)

            self.conn.commit()

            return True
        except Exception as err:
            print("Failed to add " + str(len(self._expressions_to_commit)) + " expressions", file=sys.stderr)
            print("Failed to add expression such as:" + str(self._expressions_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err


    def commit(self, force=False):
        # self.conn.autocommit = True

        if force == True or len(self._expressions_to_commit) >= MAX_UNCOMMITED:
            self.__insert_expressions_docs()
            self.__insert_expressions()
            self.__insert_long_pairs()


            self.conn.commit()
            self._positions_to_commit = []
            self._expressions_to_commit = []
            self.partial_pairs_to_commit=[]


            #self.conn.autocommit = False


    def commit_all(self):
        self.commit(force=True)
        print("Inserting Symbols")
        self.insert_symbols()
        self.symbols_to_commit = []
        print("Inserting Any Long Pairs")
        self.__insert_long_pairs()
        print("Inserting All Pairs")
        self.__insert_pairs()
        self._pairs_to_commit = []
        self.conn.commit()



    def ignorable_pair(self,pair) ->bool:

        ignore =False
        (l,r,h,v)=pair
        if r=='None'  and 'qvar' in l and h=='0' and v=='0':
            ignore=True
        #elif l=='(' and r==')' and h=='2' and v=='0':
        #    ignore=True
        print ("Deciding for pair "+str(pair) + " Ignoring: "+str(ignore))
        #ignore =False
        return ignore

    def expression_ids_for_concrete_pairs(self, expression_matches, concrete_pairs_in_query):

        for pair in concrete_pairs_in_query:
            if self.ignorable_pair(pair):
                continue

            pair_query = """ SELECT  expressions,num_expressions
                         FROM pairs p
                         JOIN symbols ls ON ls.id = p.lpair
                         JOIN symbols rs ON rs.id = p.rpair
                         where ls.symbol=%s AND rs.symbol=%s AND hdistance=%s AND vdistance=%s



                """

            self.cursor.execute(pair_query, pair)

            for pair_expressions,total_expression_count in self.cursor.fetchall():
                expression_counts = simplejson.loads(zlib.decompress(pair_expressions))
                for exp_id, cnt in expression_counts:
                    if exp_id not in expression_matches:
                        er = ExpressionRanker()
                        expression_matches[exp_id] = er
                        er.expid = exp_id
                    else:
                        er = expression_matches[exp_id]

                    er.add_concrete_pair(pair, cnt)

        return expression_matches

    def get_expressions_id_pair_for_left_wildcard_pairs(self, expression_matches, left_wildcard_pairs_in_query):
        for variable, pairs in left_wildcard_pairs_in_query.items():
            for p in pairs:
                if self.ignorable_pair(p):
                    continue

                pair_query = """ SELECT  expressions,ls.symbol
                             FROM pairs p
                             JOIN symbols rs ON rs.id = p.rpair
                             JOIN symbols ls ON ls.id = p.lpair
                             where rs.symbol=%s AND hdistance=%s AND vdistance=%s



                    """
                (lpair, rpair, hdis, vdis) = p

                self.cursor.execute(pair_query, (rpair, hdis, vdis))

                for pair_expressions,candidate in self.cursor.fetchall():
                    expression_counts = simplejson.loads(zlib.decompress(pair_expressions))

                    for exp_id, cnt in expression_counts:
                        if exp_id not in expression_matches:
                            er = ExpressionRanker()
                            expression_matches[exp_id] = er
                            er.expid = exp_id
                        else:
                            er = expression_matches[exp_id]

                        er.add_wildcard_pair(variable, candidate, (candidate, rpair, hdis, vdis), cnt)


    def get_expressons_id_pair_for_right_wildcard_pairs(self, expression_matches, right_wildcard_pairs_in_query):
        for variable, pairs in right_wildcard_pairs_in_query.items():
            for p in pairs:
                if self.ignorable_pair(p):
                    continue

                pair_query = """ SELECT  expressions,rs.symbol
                             FROM pairs p
                             JOIN symbols ls ON ls.id = p.lpair
                             JOIN symbols rs ON rs.id = p.rpair
                             where rs.symbol=%s AND hdistance=%s AND vdistance=%s

                    """
                (lpair, rpair, hdis, vdis) = p

                self.cursor.execute(pair_query, (lpair, hdis, vdis))


                for pair_expressions,candidate in self.cursor.fetchall():
                    expression_counts = simplejson.loads(zlib.decompress(pair_expressions))

                    for exp_id, cnt in expression_counts:
                        if exp_id not in expression_matches:
                            er = ExpressionRanker()
                            expression_matches[exp_id] = er
                            er.expid = exp_id
                        else:
                            er = expression_matches[exp_id]

                        er.add_wildcard_pair(variable, candidate, (lpair, candidate, hdis, vdis), cnt)


    def get_pairs_and_latex_in_expressions(self, all_expression_ids):
        if len(all_expression_ids)!=0:
            placeholder = '%s'
            placeholders = ', '.join([placeholder] * len(all_expression_ids))
            query = 'Select id,latex,fmeasure_score from expression where id in (%s)' % placeholders
            self.cursor.execute(query, list(all_expression_ids))
            rows = self.cursor.fetchall()
            for row in rows:
                id = row[0]
                latex = row[1]
                fmeasure_score = row[2]

                expr_object = all_expression_ids[id]
                """:type : ExpressionRanker"""
                expr_object.set_latex(latex)
                expr_object.set_total_pairs(fmeasure_score)


    def get_results_and_position_info(self, exp_id_latex, exp_id_scores):
        results = []
        if len(exp_id_latex)!=0:

            placeholder = '%s'
            placeholders = ', '.join([placeholder] * len(exp_id_scores))

            query = 'SElect expression_id,doc_id,positions from expression_docs where expression_id in (%s)' % placeholders
            self.cursor.execute(query, list(exp_id_scores.keys()))
            expr_doc_dict = {}
            expr_doc_positions_dict = {}

            for expr, doc, position in self.cursor.fetchall():
                expr_doc_dict[expr] = expr_doc_dict.get(expr, [])
                expr_doc_dict[expr].append(doc)
                position = simplejson.loads(position)
                expr_doc_positions_dict[expr] = expr_doc_positions_dict.get(expr, {})
                expr_doc_positions_dict[expr][doc] = position

                score = exp_id_scores[expr]
                latex = exp_id_latex[expr]
                r = Result(latex=latex, score=score, links=expr_doc_dict[expr], doc_positions=expr_doc_positions_dict[expr],
                           expr_id=expr, debug_info='')

                results.append(r)
        return results

    def search(self, search_tree):
        print("Searching database %s"%(self.db))

        """
        Return all matches for this index tree

        :type search_tree: SymbolTree
        :param search_tree:Symbol Tree


        :rtype: list[Result]
        :return: list of index results

        """

        matches = defaultdict(list)
        pair_counts = dict()
        self.cursor.execute("select count(*)from expression")
        total_exprs = self.cursor.fetchone()[0]


        # get query pairs and paths
        search_pairs, paths = search_tree.get_pairs(get_paths=True)
        total_pairs_in_query = len(search_pairs)


        # get concrete pairs,get wildcard pairs from left and right
        concrete_pairs_in_query = []
        left_wildcard_pairs_in_query = defaultdict(list)  # {"qvar":[]}
        right_wildcard_pairs_in_query = defaultdict(list)
        all_expressions = {}

        for pair in search_pairs:
            # is qvar
            (lpair, rpair, hdis, vdis) = pair.split("|")
            if "qvar" in lpair:
                _, variable = lpair.split("qvar_")
                left_wildcard_pairs_in_query[variable].append((lpair, rpair, hdis, vdis))
            elif "qvar" in rpair:
                _, variable = rpair.split("qvar_")
                right_wildcard_pairs_in_query[variable].append((lpair, rpair, hdis, vdis))
            else:  #concrete pair
                concrete_pairs_in_query.append((lpair, rpair, hdis, vdis))

        # get expressions ids for concrete pairs
        print("Getting ids for concrete pairs")
        self.expression_ids_for_concrete_pairs(all_expressions, concrete_pairs_in_query)

        #####for left wildcard pairs
        print("Getting ids for left wildcard pairs")
        self.get_expressions_id_pair_for_left_wildcard_pairs(all_expressions,
                                                             left_wildcard_pairs_in_query)
        #######for right wildcard pairs
        print("Getting ids for right wildcard pairs")
        self.get_expressons_id_pair_for_right_wildcard_pairs(all_expressions,
                                                             right_wildcard_pairs_in_query)

        #get pairs in expressions
        self.get_pairs_and_latex_in_expressions(all_expressions)

        sorted_by_most_match = sorted(all_expressions.items(), key=lambda k: k[1].possible_matched_pairs(),
                                      reverse=True)
        filtered_candidates = sorted_by_most_match[:max_results * 2]

        #score expresssions
        print("Scoring expressions")
        exp_id_scores = {}
        exp_id_latex = {}
        for exp_id, exp_obj in filtered_candidates:
            assert isinstance(exp_obj, ExpressionRanker)
            score = exp_obj.f_rank(concrete_pairs_in_query, left_wildcard_pairs_in_query, right_wildcard_pairs_in_query,
                                   total_pairs_in_query)
            exp_id_scores[exp_id] = score
            exp_id_latex[exp_id] = exp_obj.latex


        # Sort the results, and get additional information for the top results.

        num_possible_expressions = len(sorted_by_most_match)



        # get postions
        print("Getting position doc ")
        results = self.get_results_and_position_info(exp_id_latex, exp_id_scores)

        print("Soring ")
        results.sort(key=lambda x: x.score, reverse=True)
        sub_results = results[:max_results]

        print("Returning math result")
        return sub_results, num_possible_expressions, search_pairs, None


