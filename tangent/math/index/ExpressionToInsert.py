import simplejson

__author__ = 'Nidhin'


class ExpressionToInsert:
    __slots__ = [  'latex', 'num_pairs', 'all_pairs','doc_position','mathml']



    def __init__(self):
        self.latex = None
        self.num_pairs = 0
        self.all_pairs = None
        self.doc_position=None
        self.mathml=None

    def add_doc_positions(self,docid,positions):
        positions_str=simplejson.dumps(positions)
        self.doc_position=(docid,positions_str)