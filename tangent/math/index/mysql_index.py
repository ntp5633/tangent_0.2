from collections import defaultdict, Counter
import os
import sys
import hashlib
import csv
import json

import simplejson

from tangent.math.index.baseindex import BaseIndex, Result
from tangent.math.rankers.fmeasureranker import FMeasureRanker


MAX_UNCOMMITED = 10000

__author__ = 'Nidhin'

import pymysql as mdb
#import mysql.connector as mdb


class MySQLIndex(BaseIndex):
    def __init__(self, db, ranker=None, port=3306, host='127.0.0.1', process_id=-1):
        self.symboltree = {}
        conn = mdb.connect(host=host, port=port, user='math_searcher', passwd='KEfks5sxIYAA7z7QnNkg', db=db,
                           use_unicode=True,
                           charset='utf8mb4')

        self.ranker = ranker
        self.conn = conn
        self.conn.autocommit(False)

        #self.conn.autocommit = False
        self.cursor = conn.cursor()

        self._non_commited = 0
        self._expressions_to_commit = []
        self._pairs_to_commit = []
        self._positions_to_commit = []

        self.cursor.execute("SET NAMES 'utf8';  ")
        self.cursor.execute("SET CHARACTER SET utf8mb4; ")

        #self.cursor.execute("SET tx_isolation = 'READ-COMMITTED';")
        self.process_id = process_id

    def flush(self):
        self.cursor.execute("TRUNCATE  expression")
        self.cursor.execute("TRUNCATE  expression_docs")
        self.cursor.execute("TRUNCATE  pairs")
        self.cursor.execute("TRUNCATE  expr_unique")
        self.conn.commit()

    def add(self, tree):
        """
        Add symbol tree to index

        :type tree: SymbolTree
        :param tree:Symbol Tree


        """
        """
        Add symbol tree to index

        :type tree: SymbolTree
        :param tree:Symbol Tree


        """

        # Check if expression is in the index.
        (newly_inserted, expr_id) = self.exact_search(tree)
        if newly_inserted == True:
            representation = tree.build_repr()
            self.symboltree[representation] = expr_id

            pairs, _ = tree.get_pairs(get_paths=True)

            pairs_filtered = []
            for p in pairs:
                if len(p) > 200:
                    print("Did not add pair:" + p)
                else:
                    if "\n" not in p:
                        pairs_filtered.append(p)

            pairs = pairs_filtered
            pairs_list = pairs

            fmeasure_score = FMeasureRanker.search_score(pairs_list)
            self.add_expression(expr_id, latex=tree.latex, all_pairs=pairs_list, pairs=pairs_list,
                                fmeasure_score=fmeasure_score)

        self.add_expression_doc(expr_id, tree.document, tree.position)

        self._non_commited += 1
        #if self._non_commited > MAX_UNCOMMITED:
        #    self.commit()


    def exact_search(self, search_tree):
        """
        Return symbols in symbol tree in index if any

        :type search_tree:SymbolTree
        :param search_tree: symbol pair

        :rtype: string
        :return: symbol path of tree

        """
        representation_org = search_tree.build_repr()
        representation = hashlib.sha512(representation_org.encode()).hexdigest()
        if representation in self.symboltree:
            return False, self.symboltree[representation]
        else:

            self.cursor.execute("""INSERT  ignore INTO expr_unique (expression,symbol_resentation) VALUES(%s,%s) """,
                                (representation, representation_org))
            self.conn.commit()
            self.cursor.execute(
                "SELECT expr_uniquecol,expression,symbol_resentation from expr_unique where expression = %s ",[representation])
            new_id, expression, symbol_resentation = self.cursor.fetchone()

            if (symbol_resentation != representation_org):
                print("Collision %s, %s " % (symbol_resentation, representation_org), file=sys.stderr)
                sys.exit()

            self.symboltree[representation] = new_id
            return True, new_id


    def add_expression_doc(self, expression_id, doc_id, positions):
        positions = simplejson.dumps(positions)
        self._positions_to_commit.append((expression_id, doc_id, positions))


    def add_expression(self, id, latex, all_pairs, fmeasure_score, pairs):

        c = Counter(pairs)
        for (pair, count) in c.items():
            self._pairs_to_commit.append((pair, id, count))

        #all_pairs_str = jsonpickle.encode(all_pairs)
        #all_pairs_str = pickle.dumps(all_pairs)
        all_pairs_str = json.dumps(all_pairs,ensure_ascii=False)
        #all_pairs_str_compressed = zlib.compress(all_pairs_str.encode("utf-8"), 9)
        self._expressions_to_commit.append((id, latex, all_pairs_str, fmeasure_score))

    #@retry(retry_on_exception=Exception, stop_max_attempt_number=7, wait_exponential_multiplier=1000,
    #       wait_exponential_max=10000)
    def __insert_expressions_docs(self):
        try:
            # self.cursor.executemany("insert into expression_docs(expression_id,doc_id,positions) values(%s,%s,%s)"
            # "ON DUPLICATE KEY UPDATE positions=positions", self._positions_to_commit)
            #self.cursor.executemany("insert ignore expression_docs(expression_id,doc_id,positions) values(%s,%s,%s)"
            #                        , self._positions_to_commit)

            #self.conn.commit()

            filename=os.path.join(os.getcwd(), 'data',"expression_docs."+str(self.process_id))

            with open(filename, 'w', newline='',encoding='utf-8') as infile:
                writer = csv.writer( infile, delimiter='\t',quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)
                writer.writerows(self._positions_to_commit)

            return True
        except Exception as err:
            print("Failed to add " + str(len(self._positions_to_commit)) + " expression_docs", file=sys.stderr)
            print("Failed to add doc such as:" + str(self._positions_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err

    #@retry(retry_on_exception=Exception, stop_max_attempt_number=7, wait_exponential_multiplier=1000,
    #       wait_exponential_max=10000)
    def __insert_pairs(self):
        try:
            # self.cursor.executemany("insert into pairs(pair,expression_id,count) values(%s,%s,%s)"
            # "ON DUPLICATE KEY UPDATE count=count"
            # ,
            #                        self._pairs_to_commit)

            #self.cursor.executemany("insert ignore into pairs(pair,expression_id,count) values(%s,%s,%s)"
            #                        ,
            #                        self._pairs_to_commit)

            # self.cursor.execute("insert into pairs(pair,expression_id,count) values(%s,%s,%s)",
            # p)
            #self.conn.commit()


            filename=os.path.join(os.getcwd(), 'data',"pairs."+str(self.process_id))

            with open(filename, 'w', newline='',encoding='utf-8') as infile:
                writer = csv.writer( infile, delimiter='\t',quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)
                writer.writerows(self._pairs_to_commit)

            return True
        except Exception as err:
            print("Failed to add " + str(len(self._pairs_to_commit)) + " pairs", file=sys.stderr)
            print("Failed to add pairs such as:" + str(self._pairs_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err

    #@retry(retry_on_exception=Exception, stop_max_attempt_number=7, wait_exponential_multiplier=1000,
    #       wait_exponential_max=10000)
    def __insert_expressions(self):
        try:
            #self.cursor.executemany("""insert ignore into #expression(id,latex,all_pairs,fmeasure_score) values(%s,%s,%s,%s)

            #                    """,
            #                       self._expressions_to_commit)
            # self.cursor.executemany("""insert into expression(id,latex,all_pairs,fmeasure_score) values(%s,%s,%s,%s)
            # ON DUPLICATE KEY UPDATE fmeasure_score=fmeasure_score
            # """,
            #                        self._expressions_to_commit)
            #self.conn.commit()

            filename=os.path.join(os.getcwd(), 'data',"expression."+str(self.process_id))

            with open(filename, 'w', newline='',encoding='utf-8' ) as infile:
                writer = csv.writer( infile, delimiter='\t',quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)
                writer.writerows(self._expressions_to_commit)
            return True
        except Exception as err:
            print("Failed to add " + str(len(self._expressions_to_commit)) + " expressions", file=sys.stderr)
            print("Failed to add expression such as:" + str(self._expressions_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err

    def commit(self):
        self.conn.autocommit=True

        self.__insert_pairs()
        self.__insert_expressions_docs()
        self.__insert_expressions()

        self.conn.commit()
        self._positions_to_commit = []
        self._expressions_to_commit = []
        self._pairs_to_commit = []
        self._non_commited = 0
        self.conn.autocommit=False


    def commit_all(self):
        self.commit()

    def search(self, search_tree):
        max_results = 50
        """
        Return all matches for this index tree

        :type search_tree: SymbolTree
        :param search_tree:Symbol Tree


        :rtype: list[Result]
        :return: list of index results

        """

        matches = defaultdict(list)
        pair_counts = dict()
        self.cursor.execute("select count(*)from expression")
        total_exprs = self.cursor.fetchone()[0]

        search_pairs, paths = search_tree.get_pairs(get_paths=True)
        search_paths = defaultdict(list)
        for pair, path in zip(search_pairs, paths):
            search_paths[pair].append(path)
        search_pair_counts = dict(Counter(search_pairs))

        # Get expressions that contain each pair and count them.

        placeholder = '%s'
        placeholders = ', '.join([placeholder] * len(search_pair_counts.keys()))

        pair_query = """ SELECT pair,expression_id,count
                    from pairs
                    where pair in  (%s)

                """ % placeholders

        self.cursor.execute(pair_query, list(search_pair_counts.keys()))

        rows = self.cursor.fetchall()

        # if no pairs matching

        if (len(rows) == 0):
            return [], 0, search_pairs, None

        matches = {}
        for (pair, expression_id, count) in rows:

            matches[expression_id] = matches.get(expression_id, [])
            matched_count = int(count)
            if int(count) > search_pair_counts[pair]:
                matched_count = search_pair_counts[pair]

            for i in range(0, matched_count):
                matches[expression_id].append(pair)

            pair_counts[pair] = pair_counts.get(pair, 0) + 1



        # Get expression scores
        placeholder = '%s'
        placeholders = ', '.join([placeholder] * len(matches.keys()))
        query = 'Select id,fmeasure_score from expression where id in (%s)' % placeholders
        self.cursor.execute(query, list(matches.keys()))

        rows = self.cursor.fetchall()

        result_scores_dict = {}
        for (id, score) in rows:
            result_scores_dict[id] = float(score)


        # Get max score for the index term
        search_score = self.ranker.search_score(search_pairs, pair_counts,
                                                total_exprs)

        # Calculate a score for each matched expression.
        ranked_matches = []

        num_possible_expressions = len(matches.keys())
        id_final_score = {}

        for expr_id in matches.keys():
            match_pairs = matches[expr_id]
            result_score = result_scores_dict[expr_id]
            final_score = self.ranker.rank(match_pairs, search_score,
                                           result_score, None,
                                           total_exprs, search_paths)

            id_final_score[expr_id] = final_score

        top_expressions = [k for (k, v) in sorted(id_final_score.items(), key=lambda x: x[1], reverse=True)]
        top_expressions = top_expressions[0:max_results]

        # Sort the results, and get additional information for the top results.



        placeholder = '%s'
        placeholders = ', '.join([placeholder] * len(top_expressions))

        # get postions
        query = 'SElect expression_id,doc_id,positions from expression_docs where expression_id in (%s)' % placeholders
        self.cursor.execute(query, top_expressions)

        expr_doc_dict = {}
        expr_doc_positions_dict = {}
        for expr, doc, position in self.cursor.fetchall():
            expr_doc_dict[expr] = expr_doc_dict.get(expr, [])
            expr_doc_dict[expr].append(doc)
            position = simplejson.loads(position)
            expr_doc_positions_dict[expr] = expr_doc_positions_dict.get(expr, {})
            expr_doc_positions_dict[expr][doc] = position



        # get expression details
        placeholder = '%s'
        placeholders = ', '.join([placeholder] * len(top_expressions))
        query = 'Select id,latex,all_pairs from expression where id in (%s)' % placeholders
        self.cursor.execute(query, top_expressions)

        rows = self.cursor.fetchall()

        results = []
        for (id, latex, all_pairs) in rows:
            #all_pairs_fromdb = all_pairs.decode('utf-8')
            #all_pairs_decompr = zlib.decompress(all_pairs_fromdb)
            #all_pairs_obj = json.loads(all_pairs)
            all_pairs_obj = json.loads(all_pairs)
            score, matched_pairs, unmatched_pairs, unifications = self.ranker.rank2(all_pairs_obj, search_pairs)
            r = Result(latex=latex, score=score, links=expr_doc_dict[id], doc_positions=expr_doc_positions_dict[id],
                       expr_id=id, debug_info='')
            results.append(r)

        results.sort(key=lambda x: x.score, reverse=True)

        return results, num_possible_expressions, search_pairs, None
