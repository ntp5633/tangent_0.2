import copy

__author__ = 'Nidhin'


class ExpressionRanker:
    def __init__(self):
        self.expid = -1
        self.retrieved_pairs = {}
        self.wildcard_candidates = {}
        self.total_pairs = -1
        self.wildcard_candidate_pairs = {}
        self.latex = ""


    def add_concrete_pair(self, pair, count):
        self.retrieved_pairs[pair] = self.retrieved_pairs.get(pair, 0) + count


    def add_wildcard_pair(self, wildcard, candidate, pair, count):
        # ######Should only be called after add concrete pair is done

        if pair not in self.retrieved_pairs:
            self.retrieved_pairs[pair] = self.retrieved_pairs.get(pair, 0) + count
        wc = (wildcard, candidate)

        self.wildcard_candidates[wildcard] = self.wildcard_candidates.get(wildcard, set())
        self.wildcard_candidates[wildcard].add(candidate)

        self.wildcard_candidate_pairs[wc] = self.wildcard_candidate_pairs.get(wc, set())
        self.wildcard_candidate_pairs[wc].add(pair)

    def set_latex(self, latex):
        self.latex = latex

    def set_total_pairs(self, cnt):
        self.total_pairs = cnt

    def possible_matched_pairs(self):
        return sum(self.retrieved_pairs.values())


    def f_rank(self, concrete_pairs, left_wildcard_pairs, right_wildcard_pairs, total_pairs_in_query):
        gb_unmatched_pairs = copy.deepcopy(self.retrieved_pairs)
        matched_pairs = {}

        # concrete pair matching
        for p in concrete_pairs:  #for each concrete pair
            if p in gb_unmatched_pairs:  #if pair is unmatched
                matched_pairs[p] = matched_pairs.get(p, 0) + 1  #increment matched count
                gb_unmatched_pairs[p] -= 1  #decrement unmatched count
                if gb_unmatched_pairs[p] == 0:  #if pair no longere is unmatched, delete from dictionary
                    del gb_unmatched_pairs[p]

        #get number of wildcard variables
        varaibles = set(left_wildcard_pairs.keys())
        variables = varaibles.union(set(right_wildcard_pairs.keys()))

        if len(varaibles) != 0:  #there are variables
            av_varaibles = copy.deepcopy(varaibles)
            for i in range(0, len(variables)):
                max_qvar = ''
                max_binding = ''
                max_matched = 0

                for qvar in av_varaibles:
                    if qvar not in self.wildcard_candidates:
                        continue
                    candidates = self.wildcard_candidates[qvar]
                    for c in candidates:
                        unmatched_pairs = copy.deepcopy(gb_unmatched_pairs)
                        matched = 0
                        for (lpair, rpair, h, v) in left_wildcard_pairs[qvar]:
                            new_pair = (c, rpair, h, v)
                            if new_pair in unmatched_pairs:
                                unmatched_pairs[new_pair] = unmatched_pairs[new_pair] - 1
                                matched = matched + 1
                                if unmatched_pairs[new_pair] == 0:
                                    del (unmatched_pairs[new_pair])

                        for (lpair, rpair, h, v) in right_wildcard_pairs[qvar]:
                            new_pair = (lpair, c, h, v)
                            if new_pair in unmatched_pairs:
                                unmatched_pairs[new_pair] = unmatched_pairs[new_pair] - 1
                                matched = matched + 1
                                if unmatched_pairs[new_pair] == 0:
                                    del (unmatched_pairs[new_pair])

                        if matched > max_matched:
                            max_qvar = qvar
                            max_matched = matched
                            max_binding = c

                # do those bindings
                for (lpair, rpair, h, v) in left_wildcard_pairs[max_qvar]:
                    p = (max_binding, rpair, h, v)
                    if p in gb_unmatched_pairs:
                        matched_pairs[p] = matched_pairs.get(p, 0) + 1
                        gb_unmatched_pairs[p] -= 1  #decrement unmatched count
                        if gb_unmatched_pairs[p] == 0:  #if pair no longere is unmatched, delete from dictionary
                            del gb_unmatched_pairs[p]

                for (lpair, rpair, h, v) in right_wildcard_pairs[max_qvar]:
                    p = (lpair, max_binding, h, v)
                    if p in gb_unmatched_pairs:
                        matched_pairs[p] = matched_pairs.get(p, 0) + 1
                        gb_unmatched_pairs[p] -= 1  #decrement unmatched count
                        if gb_unmatched_pairs[p] == 0:  #if pair no longere is unmatched, delete from dictionary
                            del gb_unmatched_pairs[p]

                if max_qvar in av_varaibles:
                    av_varaibles.remove(max_qvar)

        matched_count = sum(matched_pairs.values())
        unmatched_count = sum(gb_unmatched_pairs.values())
        total_pairs_in_cand = matched_count + unmatched_count
        final_score = (2 * matched_count) / (total_pairs_in_query + total_pairs_in_cand)

        return final_score

