from collections import defaultdict, Counter
import io
import sys
import hashlib
import csv
import json

import simplejson

from tangent.math.index.ExpressionRanker import ExpressionRanker
from tangent.math.index.baseindex import BaseIndex, Result
from tangent.math.rankers.fmeasureranker import FMeasureRanker


MAX_UNCOMMITED = 10000
max_results = 1000
__author__ = 'Nidhin'

# import psycopg2
# import mysql.connector as mdb


class PostIndexNTCIR(BaseIndex):
    def __init__(self, db, ranker=None, port=5432, host='127.0.0.1', process_id=-1):
        self.symboltree = {}
        self.symbol_mapping = {}
        conn = psycopg2.connect(host=host, port=port, dbname=db, user='math_searcher', password='KEfks5sxIYAA7z7QnNkg')

        self.ranker = ranker
        self.conn = conn
        #self.conn.autocommit=False

        #self.conn.autocommit = False
        self.cursor = conn.cursor()

        self._non_commited = 0
        self._expressions_to_commit = []
        self._pairs_to_commit = []
        self._positions_to_commit = []

        self.__seen_exp_doc_id_pairs = set()
        self.cursor.execute("SET NAMES 'utf8';  ")

        #self.cursor.execute("SET tx_isolation = 'READ-COMMITTED';")
        self.process_id = process_id

    def flush(self):
        self.cursor.execute("TRUNCATE  math_index.expression")
        self.cursor.execute("TRUNCATE  math_index.expression_docs")
        self.cursor.execute("TRUNCATE  math_index.pairs")
        self.cursor.execute("TRUNCATE  math_index.expr_unique")
        self.conn.commit()

    def add(self, tree):
        """
        Add symbol tree to index

        :type tree: SymbolTree
        :param tree:Symbol Tree


        """
        """
        Add symbol tree to index

        :type tree: SymbolTree
        :param tree:Symbol Tree


        """

        # Check if expression is in the index.
        (newly_inserted, expr_id) = self.exact_search(tree)
        if newly_inserted == True:
            representation = tree.build_repr()
            self.symboltree[representation] = expr_id

            pairs, paths = tree.get_pairs(get_paths=True)

            #remove "invalid" pairs and process
            pairs_filtered = []
            pairs_filtered_unprocessed = []
            for p in pairs:
                if len(p) > 500:  #symbol can be up to 180 chars in matrix
                    print("Did not add pair: %s" % (p), file=sys.stderr)
                else:
                    (lpair, rpair, hdis, vdis) = p.split("|")
                    pairs_filtered_unprocessed.append(p)
                    lpair_id = self.get_symbol_id(lpair)
                    rpair_id = self.get_symbol_id(rpair)
                    pair_tuple = lpair_id, rpair_id, hdis, vdis
                    pairs_filtered.append(pair_tuple)

            pairs = pairs_filtered
            pairs_list = pairs

            fmeasure_score = FMeasureRanker.search_score(pairs_list)
            self.add_expression(expr_id, latex=tree.latex, all_pairs=pairs_filtered_unprocessed, pairs=pairs_list,
                                fmeasure_score=fmeasure_score)

        if ((expr_id, tree.document) not in self.__seen_exp_doc_id_pairs):
            self.add_expression_doc(expr_id, tree.document, tree.position)
            self.__seen_exp_doc_id_pairs.add((expr_id, tree.document))

        self._non_commited += 1
        #if self._non_commited > MAX_UNCOMMITED:
        #    self.commit()


    def exact_search(self, search_tree):
        """
        Return symbols in symbol tree in index if any

        :type search_tree:SymbolTree
        :param search_tree: symbol pair

        :rtype: string
        :return: symbol path of tree

        """
        need_to_insert = False;
        representation_org = search_tree.build_repr()
        representation = hashlib.sha512(representation_org.encode()).hexdigest()
        if representation in self.symboltree:
            return need_to_insert, self.symboltree[representation]
        else:

            try:
                self.cursor.execute(
                    """INSERT  INTO math_index.expr_unique (expression,symbol_resentation) Values(%s,%s)""",
                    (representation, representation_org))
                self.conn.commit()
                need_to_insert = True
            except psycopg2.IntegrityError as err:
                need_to_insert = False
                self.conn.rollback()

            self.cursor.execute(
                "SELECT expr_uniquecol,expression,symbol_resentation from math_index.expr_unique where expression = %s ",
                [representation])
            new_id, expression, symbol_resentation = self.cursor.fetchone()

            if (symbol_resentation != representation_org):
                print("Collision %s, %s " % (symbol_resentation, representation_org), file=sys.stderr)
                sys.exit()

            self.symboltree[representation] = new_id
            return need_to_insert, new_id


    def get_symbol_id(self, symbol):
        if symbol in self.symbol_mapping:
            return self.symbol_mapping[symbol]
        else:

            self.cursor.execute("""INSERT  INTO math_index.symbols (symbol) SELECT %s  WHERE not exists
            (SELECT 1 FROM math_index.symbols WHERE symbol = %s)""", (symbol, symbol))

            self.conn.commit()
            self.cursor.execute(
                "SELECT id from math_index.symbols where symbol = %s ", [symbol])

            new_id = self.cursor.fetchone()[0]
            self.symbol_mapping[symbol] = new_id
            return new_id

    def add_expression_doc(self, expression_id, doc_id, positions):
        positions = simplejson.dumps(positions)
        self._positions_to_commit.append((int(expression_id), int(doc_id), positions))


    def add_expression(self, id, latex, all_pairs, fmeasure_score, pairs):

        c = Counter(pairs)
        for (pair, count) in c.items():
            lpair, rpair, hdis, vdis = pair
            self._pairs_to_commit.append((lpair, rpair, int(hdis), int(vdis), int(id), int(count)))

        #all_pairs_str = jsonpickle.encode(all_pairs)
        #all_pairs_str = pickle.dumps(all_pairs)
        all_pairs_str = json.dumps(all_pairs, ensure_ascii=False)
        #all_pairs_str_compressed = zlib.compress(all_pairs_str.encode("utf-8"), 9)
        self._expressions_to_commit.append((int(id), latex, all_pairs_str, fmeasure_score))

    #@retry(retry_on_exception=Exception, stop_max_attempt_number=7, wait_exponential_multiplier=1000,
    #       wait_exponential_max=10000)
    def __insert_expressions_docs(self):
        try:
            #self.cursor.executemany("insert into expression_docs(expression_id,doc_id,positions) values(%s,%s,%s)" +
            # "ON DUPLICATE KEY UPDATE positions=positions", self._positions_to_commit)
            #self.cursor.executemany("insert ignore expression_docs(expression_id,doc_id,positions) values(%s,%s,%s)"
            #                        , self._positions_to_commit)

            #self.conn.commit()

            """
            filename=os.path.join(os.getcwd(), 'data',"expression_docs."+str(self.process_id))

            with open(filename, 'w', newline='',encoding='utf-8') as infile:
                writer = csv.writer( infile, delimiter='\t',quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)
                writer.writerows(self._positions_to_commit)

            """

            filename = io.StringIO()
            sorted_pairs = sorted(self._positions_to_commit)
            writer = csv.writer(filename, delimiter='\t', quotechar='\'', lineterminator='\n',
                                quoting=csv.QUOTE_NONNUMERIC)
            writer.writerows(sorted_pairs)
            filename.seek(0)
            self.cursor.copy_from(filename, 'math_index.expression_docs',
                                  columns=('expression_id', 'doc_id', 'positions'))

            #return True
        except Exception as err:
            print("Failed to add " + str(len(self._positions_to_commit)) + " expression_docs", file=sys.stderr)
            print("Failed to add doc such as:" + str(self._positions_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err

    #@retry(retry_on_exception=Exception, stop_max_attempt_number=7, wait_exponential_multiplier=1000,
    #       wait_exponential_max=10000)
    def __insert_pairs(self):
        try:
            # self.cursor.executemany("insert into pairs(pair,expression_id,count) values(%s,%s,%s)"
            # "ON DUPLICATE KEY UPDATE count=count"
            # ,
            #                        self._pairs_to_commit)

            #self.cursor.executemany(
            #    "insert ignore into pairs(lpair,rpair,hdistance,vdistance,expression_id,count) values(%s,%s,%s,%s,%s,%s)"
            #    ,
            #    self._pairs_to_commit)

            #    self.conn.commit()

            filename = io.StringIO()
            sorted_pairs = sorted(self._pairs_to_commit)
            writer = csv.writer(filename, delimiter='\t', quotechar='\'', lineterminator='\n',
                                quoting=csv.QUOTE_NONNUMERIC)
            writer.writerows(sorted_pairs)
            filename.seek(0)
            self.cursor.copy_from(filename, 'math_index.pairs',
                                  columns=('lpair', 'rpair', 'hdistance', 'vdistance', 'expression_id', 'count'))

        except Exception as err:
            print("Failed to add " + str(len(self._pairs_to_commit)) + " pairs", file=sys.stderr)
            print("Failed to add pairs such as:" + str(self._pairs_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err

    #@retry(retry_on_exception=Exception, stop_max_attempt_number=7, wait_exponential_multiplier=1000,
    #       wait_exponential_max=10000)
    def __insert_expressions(self):
        try:
            sorted_pairs = sorted(self._expressions_to_commit)

            self.cursor.executemany("""insert into math_index.expression(id,latex,all_pairs,fmeasure_score) values(%s,%s,%s,%s)

                                """,
                                    sorted_pairs)
            # self.cursor.executemany(
            #     """insert ignore into expression(id,latex,all_pairs,fmeasure_score) values(%s,%s,%s,%s) """,
            #     self._expressions_to_commit)

            #self.conn.commit()
            #filename=os.path.join(os.getcwd(), 'data',"expression."+str(self.process_id))

            #with open(filename, 'w', newline='',encoding='utf-8' ) as infile:
            #writer = csv.writer( infile, delimiter='\t',quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)
            #writer.writerows(self._expressions_to_commit)



            #filename = io.StringIO()

            #writer = csv.writer( filename, delimiter='`',quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_NONNUMERIC)
            #writer.writerows(sorted_pairs)
            #filename.seek(0)
            #self.cursor.copy_from(filename,'math_index.expression',columns=('id','latex','all_pairs','fmeasure_score'),sep="`")
            #writer = csv.writer( filename, delimiter='\t',escapechar='\'', lineterminator='\n')
            #writer.writerows(sorted_pairs)
            #filename.seek(0)
            #self.cursor.copy_expert("""COPY math_index.expression FROM STDIN DELIMITER  '\t' escape  '\' """,filename)

            #  return True
        except Exception as err:
            print("Failed to add " + str(len(self._expressions_to_commit)) + " expressions", file=sys.stderr)
            print("Failed to add expression such as:" + str(self._expressions_to_commit[0]), file=sys.stderr)
            print(str(err), file=sys.stderr)
            raise err

    def commit(self):
        #self.conn.autocommit = True

        self.__insert_pairs()
        self.__insert_expressions_docs()
        self.__insert_expressions()

        self.conn.commit()
        self._positions_to_commit = []
        self._expressions_to_commit = []
        self._pairs_to_commit = []
        self._non_commited = 0
        #self.conn.autocommit = False


    def commit_all(self):
        self.commit()

    def expression_ids_for_concrete_pairs(self, expression_matches, concrete_pairs_in_query):

        for pair in concrete_pairs_in_query:
            pair_query = """ SELECT  expression_id,count(*)
                         FROM math_index.pairs p
                         JOIN math_index.symbols ls ON ls.id = p.lpair
                         JOIN math_index.symbols rs ON rs.id = p.rpair
                         where ls.symbol=%s AND rs.symbol=%s AND hdistance=%s AND vdistance=%s
                         GROUP BY expression_id


                """

            self.cursor.execute(pair_query, pair)
            rows = self.cursor.fetchall()

            for row in rows:
                exp_id = row[0]
                cnt = row[1]
                if exp_id not in expression_matches:
                    er = ExpressionRanker()
                    expression_matches[exp_id] = er
                    er.expid = exp_id
                else:
                    er = expression_matches[exp_id]

                er.add_concrete_pair(pair, cnt)

        return expression_matches

    def get_expressions_id_pair_for_left_wildcard_pairs(self, expression_matches, left_wildcard_pairs_in_query):
        for variable, pairs in left_wildcard_pairs_in_query.items():
            for p in pairs:

                pair_query = """ SELECT  expression_id,ls.symbol,count(*)
                             FROM math_index.pairs p
                             JOIN math_index.symbols rs ON rs.id = p.rpair
                             JOIN math_index.symbols ls ON ls.id = p.lpair
                             where rs.symbol=%s AND hdistance=%s AND vdistance=%s
                             GROUP BY expression_id,ls.symbol


                    """
                (lpair, rpair, hdis, vdis) = p

                self.cursor.execute(pair_query, (rpair, hdis, vdis))
                rows = self.cursor.fetchall()

                for row in rows:
                    exp_id = row[0]
                    candidate = row[1]
                    count = row[2]
                    if exp_id not in expression_matches:
                        er = ExpressionRanker()
                        expression_matches[exp_id] = er
                        er.expid = exp_id
                    else:
                        er = expression_matches[exp_id]
                    er.add_wildcard_pair(variable, candidate, (candidate, rpair, hdis, vdis), count)


    def get_expressons_id_pair_for_right_wildcard_pairs(self, expression_matches, right_wildcard_pairs_in_query):
        for variable, pairs in right_wildcard_pairs_in_query.items():
            for p in pairs:

                pair_query = """ SELECT  expression_id,rs.symbol,count(*)
                             FROM math_index.pairs p
                             JOIN math_index.symbols ls ON ls.id = p.lpair
                             JOIN math_index.symbols rs ON rs.id = p.rpair
                             where ls.symbol=%s AND hdistance=%s AND vdistance=%s
                             GROUP BY expression_id,rs.symbol

                    """
                (lpair, rpair, hdis, vdis) = p

                self.cursor.execute(pair_query, (lpair, hdis, vdis))
                rows = self.cursor.fetchall()

                for row in rows:
                    exp_id = row[0]
                    candidate = row[1]
                    count = row[2]
                    er = None
                    if exp_id not in expression_matches:
                        er = ExpressionRanker()
                        expression_matches[exp_id] = er
                        er.expid = exp_id
                    else:
                        er = expression_matches[exp_id]
                    er.add_wildcard_pair(variable, candidate, (lpair, candidate, hdis, vdis), count)

    def get_pairs_and_latex_in_expressions(self, all_expression_ids):
        placeholder = '%s'
        placeholders = ', '.join([placeholder] * len(all_expression_ids))
        query = 'Select id,latex,fmeasure_score from math_index.expression where id in (%s)' % placeholders
        self.cursor.execute(query, list(all_expression_ids))
        rows = self.cursor.fetchall()
        for row in rows:
            id = row[0]
            latex = row[1]
            fmeasure_score = row[2]

            expr_object = all_expression_ids[id]
            """:type : ExpressionRanker"""
            expr_object.set_latex(latex)
            expr_object.set_total_pairs(fmeasure_score)


    def search(self, search_tree):

        """
        Return all matches for this index tree

        :type search_tree: SymbolTree
        :param search_tree:Symbol Tree


        :rtype: list[Result]
        :return: list of index results

        """

        matches = defaultdict(list)
        pair_counts = dict()
        self.cursor.execute("select count(*)from math_index.expression")
        total_exprs = self.cursor.fetchone()[0]


        #get query pairs and paths
        search_pairs, paths = search_tree.get_pairs(get_paths=True)
        total_pairs_in_query = len(search_pairs)


        #get concrete pairs,get wildcard pairs from left and right
        concrete_pairs_in_query = []
        left_wildcard_pairs_in_query = defaultdict(list)  #{"qvar":[]}
        right_wildcard_pairs_in_query = defaultdict(list)
        all_expressions = {}

        expression_id_matched_pairs = {}
        expression_id_unmatched_pairs = {}

        expressions_wildcard_candidate = {}  #{expression: dict{}......} dict in turn {"wildcard":list of symbols

        for pair in search_pairs:
            #is qvar
            (lpair, rpair, hdis, vdis) = pair.split("|")
            if "qvar" in lpair:
                _, variable = lpair.split("qvar_")
                left_wildcard_pairs_in_query[variable].append((lpair, rpair, hdis, vdis))
            elif "qvar" in rpair:
                _, variable = rpair.split("qvar_")
                right_wildcard_pairs_in_query[variable].append((lpair, rpair, hdis, vdis))
            else:  #concrete pair
                concrete_pairs_in_query.append((lpair, rpair, hdis, vdis))

        #get expressions ids for concrete pairs
        print("Getting ids for concrete pairs")
        self.expression_ids_for_concrete_pairs(all_expressions, concrete_pairs_in_query)

        #####for left wildcard pairs
        print("Getting ids for left wildcard pairs")
        self.get_expressions_id_pair_for_left_wildcard_pairs(all_expressions,
                                                             left_wildcard_pairs_in_query)
        #######for right wildcard pairs
        print("Getting ids for right wildcard pairs")
        self.get_expressons_id_pair_for_right_wildcard_pairs(all_expressions,
                                                             right_wildcard_pairs_in_query)

        #get pairs in expressions
        self.get_pairs_and_latex_in_expressions(all_expressions)

        sorted_by_most_match = sorted(all_expressions.items(), key=lambda k: k[1].possible_matched_pairs(),
                                      reverse=True)
        filtered_candidates = sorted_by_most_match[:2000]

        #score expresssions
        print("Scoring expressions")
        exp_id_scores = {}
        exp_id_latex = {}
        for exp_id, exp_obj in filtered_candidates:
            assert isinstance(exp_obj, ExpressionRanker)
            score = exp_obj.f_rank(concrete_pairs_in_query, left_wildcard_pairs_in_query, right_wildcard_pairs_in_query,
                                   total_pairs_in_query)
            exp_id_scores[exp_id] = score
            exp_id_latex[exp_id] = exp_obj.latex


        # Sort the results, and get additional information for the top results.

        num_possible_expressions = len(sorted_by_most_match)

        placeholder = '%s'
        placeholders = ', '.join([placeholder] * len(exp_id_scores))

        # get postions
        print("Getting position doc ")
        query = 'SElect expression_id,doc_id,positions from math_index.expression_docs where expression_id in (%s)' % placeholders
        self.cursor.execute(query, list(exp_id_scores.keys()))

        expr_doc_dict = {}
        expr_doc_positions_dict = {}
        results = []
        for expr, doc, position in self.cursor.fetchall():
            expr_doc_dict[expr] = expr_doc_dict.get(expr, [])
            expr_doc_dict[expr].append(doc)
            position = simplejson.loads(position[1:-1])
            expr_doc_positions_dict[expr] = expr_doc_positions_dict.get(expr, {})
            expr_doc_positions_dict[expr][doc] = position

            score = exp_id_scores[expr]
            latex = exp_id_latex[expr]
            r = Result(latex=latex, score=score, links=expr_doc_dict[expr], doc_positions=expr_doc_positions_dict[expr],
                       expr_id=expr, debug_info='')

            results.append(r)

        print("Soring ")
        results.sort(key=lambda x: x.score, reverse=True)
        sub_results = results[:max_results]

        print("Returning math result")
        return sub_results, num_possible_expressions, search_pairs, None
