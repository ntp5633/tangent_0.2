import os
import pickle
import subprocess
import sys
import platform
__author__ = 'Nidhin'

import requests as req


class LatexToMathML(object):
    @classmethod
    def convert_to_mathml(cls, tex_query):
        os_=platform.system()
        qvar_template_file = os.path.join(os.getcwd(),"tangent","math","mathml","mws.sty.ltxml")

        if not os.path.exists(qvar_template_file):
            sys.exit("Stylesheet for wildcard is missing")

        use_shell=True
        if 'Linux' in os_:
            use_shell=False
        p2 = subprocess.Popen(['latexmlmath' ,'--pmml=-','--preload=amsmath', '--preload='+qvar_template_file, '-'], shell=use_shell, stdout=subprocess.PIPE, stdin=subprocess.PIPE,stderr=open(os.devnull, 'r'))

        (output, err) = p2.communicate(input=tex_query.encode())

        #print ("IN LATEXTOMATHML")
        #print (err)
        #print(output)
        try:
            result= output.decode('utf-8')
            return result
        except UnicodeDecodeError as uae:
            print("Failed to decode " + uae.reason, file=sys.stderr)
            result=output.decode('utf-8','replace')

        print ("Decoded %s" % result)
        return result


class ServerLatexToMathML(object):
    @classmethod
    def convert_to_mathml(cls, tex_query):
        url = 'http://halifax.cs.rit.edu:8324/'

        #tex=tex_query

        # image to query
        #pickled data
        #data = pickle.dumps(tex)

        #make equest
        r = req.get(url, data=tex_query)
        #load the numpy array
        #output = pickle.loads(r.text)

        output=r.text

        #p = subprocess.Popen('latexmlmath -pmml - -', shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE,
        #                     stderr=open(os.devnull, 'r'))
        #(output, _) = p.communicate(input=tex)
        #f = StringIO.StringIO(output)

        return output