from bs4 import BeautifulSoup
from tangent.math.models.symboltree import SymbolTree

__author__ = 'Nidhin'

"""
A test file to determine why for some equations , the similarity score of the equation in a document and
the one stored in a database is not one
"""


def get_query(query_file:str) -> str:
    parsed = None
    with open(query_file, encoding='utf-8') as file:
        parsed = BeautifulSoup(file)

    query_obj = parsed.find_all("topic")[0]

    query_num = query_obj.num.text
    query_string = ""
    # get formulas
    f = query_obj.findAll("formula")[0]
    tex = f.find("m:math").find("m:annotation").text
    return tex


eqn_latex = "{\\displaystyle W(2,k)>2^{k}/k^{\\varepsilon}}"
query_file = "E:\\tanget-update\\tangent\\ntcir\\query1.xml"

query_latex = get_query(query_file)









