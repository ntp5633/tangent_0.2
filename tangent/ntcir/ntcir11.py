from concurrent.futures import ProcessPoolExecutor
import csv
import os
from sys import argv
import sys

from bs4 import BeautifulSoup

from tangent.common.hits_generation.fragment_generator import BaseFragmentGenerator

from tangent.formatter.output_formtter import NTCIR_Raw_OutputFormatter
from tangent.server import mathsearch_helper


sys.setrecursionlimit(10000)

"""
    The main application that given an nticr query file, queries the collection and returns the results

"""


def print_help_and_exit():
    """
    Prints usage statement
    """

    exit('Usage: python ntcir11.py <name of run>  <weighing stratergy> <file containing ntcir queries>  <system>' +
         "<name of run> : can be anythin \n "
         "<weighing stratergy> : 'math_only,''math_focused','ntcir_default', 'math_text_equal' \n"
         "<system>: 'Wikipedia', 'Ntcir'"

    )


def process_query(args):
    """
    Given a query, queries the databse and saves the result tp a file
    :param args:
    :return:
    """
    ntcir_main_count = 1000  # main task require 1000 results retruend
    ntcir_wiki_count = 100

    run_tag, strategy, (query_num, query_string), file_mapping, system = args
    if system is None:
        system = 'NTCIR Actual'
    additional_options = {}
    additional_options['query_id'] = query_num
    additional_options['run_tag'] = run_tag
    additional_options['system'] = system
    additional_options['file_mapping'] = file_mapping
    additional_options['min_count'] = ntcir_wiki_count

    ms = mathsearch_helper.MathSearcher(system=system, query=query_string, weight=strategy)
    response = ms.combined_search(max_results=1000, fragment_generator=BaseFragmentGenerator)

    output_str = NTCIR_Raw_OutputFormatter.process(response, additional_options)
    filename = "%s_%s.tsv" % (strategy, query_num)
    file_path = os.path.join("results", filename)
    with open(file_path, mode='w', encoding='utf-8') as file:
        file.write(output_str)

    return query_num, query_string


def process_query_wiki(args):
    """
    Given a query, only queries the math  index
    :param args:
    :return:
    """
    ntcir_wiki_count = 100

    run_tag, strategy, (query_num, query_string), file_mapping, system = args
    if system is None:
        system = 'NTCIR Actual'
    additional_options = {}
    additional_options['query_id'] = query_num
    additional_options['run_tag'] = run_tag
    additional_options['system'] = system
    additional_options['file_mapping'] = file_mapping
    additional_options['min_count'] = ntcir_wiki_count

    try:
        ms = mathsearch_helper.MathSearcher(system=system, query=query_string, weight=strategy)
        response = ms.math_search_expressions_wikipedia(query_string)
        """:type: list of [Result]"""

        filename = "%s_%s.tsv" % (strategy, query_num)
        file_path = os.path.join("results", filename)

        with open(file_path, mode='w', encoding='utf-8', newline='') as file:
            writer = csv.writer(file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

            for r in response:
                r = r
                """:type : Result """
                # queryid,expr_id,docid,score,latex

                for d in r.links:
                    writer.writerow([query_num, r.expr_id, d, r.score, r.latex])

    except Exception as e:
        print("error for " + query_num)

    return query_num, query_string


def get_query_string(query_obj):
    """
    Parsed the query object in xml and get the expression latex and text
    :param query_obj:
    :return:
    """
    query_num = query_obj.num.text
    query_string = ""
    # get formulas
    for f in query_obj.findAll("formula"):
        tex = f.find("m:math").find("m:annotation").text
        tex_with_quotes = "$" + tex + "$"
        query_string = query_string + " " + tex_with_quotes

    # get keywords
    for k in query_obj.findAll("keyword"):
        word = k.text.strip()  # get word and strip whitespace
        query_string = query_string + " " + word

    return query_num, query_string


def process_queries(query_file, run_tag, weighting_stratergy, filemapping, system):
    """
    Parses the query file and runs the query
    :param query_file:
    :param run_tag:
    :param weighting_stratergy:
    :param filemapping:
    :param system:
    :return:
    """

    parsed = None
    with open(query_file, encoding='utf-8') as file:
        parsed = BeautifulSoup(file)

    query_list = parsed.find_all("topic")
    print("There are %s queries." % (len(query_list)))

    query_id_base = "NTCIR11−Math−"

    query_list_m = list(query_list)
    args = [(run_tag, weighting_stratergy, get_query_string(qobj), filemapping, system) for qobj in query_list_m]

    func = process_query

    if system == 'Wikipedia':
        func = process_query_wiki



        # with ProcessPoolExecutor() as executor:
    with ProcessPoolExecutor() as executor:
        for query_num, query_string in executor.map(func, args):
            print("Processed %s %s" % (query_num, query_string))


if __name__ == '__main__':
    if len(argv) > 1:
        if argv[1] == 'help':
            print_help_and_exit()
        query_file = argv[1]  # full path of query file

        group_name = 'rit_'

        run_tag = group_name + argv[2]  # can be anything
        weighting_stratergy = argv[3]
        file_mapping = argv[4]
        system = argv[5]

        if weighting_stratergy != 'math_only' and weighting_stratergy != 'math_focused' and weighting_stratergy != 'ntcir_default' and weighting_stratergy != 'math_text_equal':
            print("Not a valid option", file=sys.stderr)
            sys.exit(-1)

        process_queries(query_file, run_tag, weighting_stratergy, file_mapping, system)

    else:
        print_help_and_exit()