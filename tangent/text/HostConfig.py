__author__ = 'Nidhin'


class SolrConfig(object):
    """
    Uses the default ranker: FMeasureConfig and default database
    """
    host = "http://localhost:8983/solr"


class NTCIRSolrConfig(SolrConfig):
    """
    Uses the FMeasureRanker and default database
    """
    host = "http://localhost:8983/solr/ntcir"


class NTCIRTestSolrConfig(SolrConfig):
    """
    Uses the FMeasureRanker and default database
    """
    host = "http://localhost:8983/solr/ntcir_test"


class WikipediaSolrConfig(SolrConfig):
    """
    Uses the FMeasureRanker and default database
    """
    host = "http://localhost:8983/solr/wikipedia"


class MathStackExchangeSolrConfig:
    host = "http://localhost:8983/solr/math_stackexchange"

class MrecSolrConfig(SolrConfig):
    """
    Uses the FMeasureRanker and default database
    """
    host = "http://localhost:8983/solr/mrec"