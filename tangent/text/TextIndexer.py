from tangent.text.models import Document

__author__ = 'Nidhin'

import pysolr


max_uncommited = 100


class TextIndexer(object):
    connection = None
    docs_to_do_add = []

    def __init__(self, config):
        """
        :type config: SolrConfig
        :param config: solr host info
        """
        self.connection = pysolr.Solr(config.host, timeout=50000)


    def add(self, doc):
        assert isinstance(doc, Document)
        self.docs_to_do_add.append(
            {
                'id': doc.id,
                'title': doc.title,
                'math_content': doc.math_content,
                'file_location': doc.file_location

            }
        )

        if len(self.docs_to_do_add) > max_uncommited:
            self.connection.add(self.docs_to_do_add)
            self.docs_to_do_add = []

    def finalize(self):
        if len(self.docs_to_do_add) > 0:
            self.connection.add(self.docs_to_do_add)
            self.docs_to_do_add = []


    def commitall(self):
        self.connection.commit()
        self.connection.optimize()


    def flush(self):
        self.connection.delete(q='*:*')
        self.connection.commit()

