import codecs
from concurrent.futures import ProcessPoolExecutor
import csv
import json
import multiprocessing
from sys import argv
import sys
import time

import tangent.text.HostConfig as hc
from tangent.text.TextIndexer import TextIndexer
from tangent.text.xml_extractor import GenericXMLExtractor, MRecXMLExtractor
from tangent.text.models import Document

max_uncommited = 100000

__author__ = 'Nidhin'


def print_help_and_exit():
    """
    Prints usage statement
    """

    exit('Usage: python text_indexing.py [NTCIR] doc_mapping  ')





def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    counter=0
    for i in range(0, len(l), n):
        yield counter,l[i:i + n]
        counter=counter+1


def text_indexer_task(pargs):
    connection,mappings,chunkid,parser_class = pargs
    name_id = chunkid
    text_indexer = TextIndexer(connection)


    print("Process %s has %s elements to process" % (name_id, len(mappings)))

    print ("Process %s  first element: %s"% (name_id,mappings[0]))
    print ("Process %s  last element: %s"% (name_id,mappings[-1]))

    for doc_path, doc_id,title in mappings:
        try:
            doc = parser_class.parse_document(doc_path)
            assert isinstance(doc, Document)
            doc.file_location = doc_path
            doc.id = doc_id
            doc.title = title

            if (doc.math_content!=None and doc.math_content.strip()!=""):
                text_indexer.add(doc)


        except Exception as err:
            print("Failed to process document " + doc_path, file=sys.stderr)
            print(str(err), file=sys.stderr)

    #with lock:
    try:
        text_indexer.finalize()
    except Exception as err:
         print("Process error " + name_id, file=sys.stderr)
         print(str(err), file=sys.stderr)

    return name_id




if __name__ == '__main__':
    if len(argv) > 2:
        model = argv[1]
        path = argv[2]
        start =time.time()
        connection = None
        text_indexer = None

        parser_class = None
        if model == "ntcir":
            connection = hc.NTCIRSolrConfig()
            text_indexer = TextIndexer(connection)
            parser_class = GenericXMLExtractor()
        elif model == "mrec":
            connection = hc.MrecSolrConfig
            text_indexer = TextIndexer(connection)
            parser_class = MRecXMLExtractor()

        elif model == "ntcir_test":
            connection = hc.NTCIRTestSolrConfig
            text_indexer = TextIndexer(connection)
            parser_class = GenericXMLExtractor()

        elif model == "wikipedia":
            connection = hc.WikipediaSolrConfig
            text_indexer = TextIndexer(connection)
            parser_class = GenericXMLExtractor()
        elif model == "math_stackexchange":
            connection = hc.MathStackExchangeSolrConfig
            text_indexer = TextIndexer(connection)
            parser_class = GenericXMLExtractor()
        else:
            print("Not recognized collection")
            sys.exit(-1)

        with codecs.open(path, 'r', "utf-8") as myfile:
            content = myfile.read().replace('\n', ' ')


        doc_mapping = []
        with open(path, newline='',encoding='utf-8') as mapping_file:

            reader = csv.reader(mapping_file,  delimiter='\t',quotechar='\'', lineterminator='\n', quoting=csv.QUOTE_ALL)

            for row in reader:
                doc_mapping.append(row)

        print("Documents to add: {0}".format(len(doc_mapping)))

        num_docs_added = 0



        max_jobs =2
        chunk_size = 1000
        manager = multiprocessing.Manager()

        num_docs=len(doc_mapping)
        num_docs_remaining=num_docs

        args = [(connection,small_chunk,chunkid,parser_class) for chunkid,small_chunk in chunks(doc_mapping, chunk_size)]


        text_indexer = TextIndexer(connection)
        text_indexer.flush()


        with ProcessPoolExecutor(max_workers=max_jobs) as executor:
            for pid in executor.map(text_indexer_task, args):
                print("%s is done parsing and saving to file" % (pid))
                #save_to_index(pid,database_name)
                print("%s is done saving to database" % (pid))
                num_docs_remaining=max(num_docs_remaining-chunk_size,0)
                print("Remaing docs %s of %s" % (num_docs_remaining,num_docs))

        print("Final commit and optimizing")
        text_indexer.commitall()


        """
        for doc_path, doc_id,title in doc_mapping:
            try:
                doc = parser_class.parse_document(doc_path)
                assert isinstance(doc, Document)
                doc.file_location = doc_path
                doc.id = doc_id
                doc.title = title
                text_indexer.add(doc)
                num_docs_added = num_docs_added + 1
                if num_docs_added % max_uncommited == 0:
                    print("Added {0} docs so far".format(num_docs_added))
            except Exception as err:
                print("Failed to process document " + doc_path, file=sys.stderr)
                print(str(err), file=sys.stderr)
        text_indexer.finalize()
        """

        print("Finished added all docs")


        end=time.time()
        elapsed=end-start

        print("Elapsed time %s"%(elapsed))
    else:
        print(argv)
        print_help_and_exit()


