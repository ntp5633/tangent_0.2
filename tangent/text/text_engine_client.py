from tangent.common.search.model.TextResult import TextDocumentHeader

__author__ = 'Nidhin'

import requests


def get_results(query,system):
    """
    :rtype: TextDocumentHeader
    :return text result info
    """
    print("Text Query")
    payload = {'q': query}
    url="http://localhost:8983/solr/"+system+"/mathtvrh"
    print ("Url %s", url)
    print ("Payload %s", payload)
    r = requests.get(url, params=payload)


    json_data = r.json()
    tdh = TextDocumentHeader(json_data, query)
    return tdh



