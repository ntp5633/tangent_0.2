__author__ = 'jshoun01'


"""
 Does not processing
"""
class RawInputFormatter:

    """
        :type input str
        :rtype str
    """
    @staticmethod
    def process(query_string) -> str:
        return query_string.strip()


"""
 gives latex query
"""
class NTCIR_Raw_InputFormatter:

    """
       :type input str
       :rtype str
    """
    @staticmethod
    def process(input) -> str:
        return input
