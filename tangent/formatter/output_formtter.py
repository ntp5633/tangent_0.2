"""
 Does not processing
"""
import jsonpickle
from tangent.common.hits_generation import HitResponse
from tangent.common.search.model import Hit


class RawOutputFormatter:
    @staticmethod
    def process(results, optional_args=None) -> HitResponse:
        return results


class JsonOutputFormatter:
    """
        :type results HitResponse
        :rtype str  str
    """

    @staticmethod
    def process(results, optional_args=None) -> str:
        response_json = jsonpickle.encode(results)
        return response_json


"""
 gives latex query
"""


class NTCIR_Raw_OutputFormatter:
    @staticmethod
    def process_ntcir_file_name(filename, system):
        if system == 'Wikipedia':
            filename = filename[filename.index('v3'):]
        else:
            filename = filename[filename.index('xhtml'):]
            filename = filename.replace("E:\\", "")
            filename = filename.replace("\\", "/")
            filename = filename.replace("xhtml5", "xhtml")

        return filename

    @staticmethod
    def format_output_tuple(filename, num, rank, runtag, score):
        return "%s\t%s\t%s\t%s\t%s\t%s" % (num, 1, filename, rank + 1, score, runtag)

    @staticmethod
    def add_extra_results_if_necessary(query_id,system=None,seen_files=None,output=None,runtag=None):
        if (system!='Wikipedia'):
            raise Exception("Don't know how to add nonsense results fro non ntc-r wikipedia")

        for i in range(0,4000):
            if len(seen_files)==1000:
                return output
            file_name="v3/{:0>5d}.html".format(i)
            if file_name not in seen_files:
                formmated_result=NTCIR_Raw_OutputFormatter.format_output_tuple(file_name, query_id, len(seen_files), runtag, 0)
                seen_files.add(file_name)
                output.append(formmated_result)

    @staticmethod
    def process(results, optional_args) -> str:
        """
        :type results: HitResponse
        :param results:
        :param optional_args:
        :return:
        """


        num = optional_args['query_id']

        results_list = results.hit_list

        runtag = optional_args['run_tag']

        system = optional_args['system']
        min_count = optional_args['min_count']
        file_mapping = optional_args['file_mapping']

        seen_files=set()
        output = []

        for rank, r in enumerate(results_list):

            filename = r.full_path

            filename = NTCIR_Raw_OutputFormatter.process_ntcir_file_name(filename, system)
            #for wikipedia

            seen_files.add(filename)
            score = r.final_score

            formmated_result = NTCIR_Raw_OutputFormatter.format_output_tuple(filename, num, rank, runtag, score)
            output.append(formmated_result)
        NTCIR_Raw_OutputFormatter.add_extra_results_if_necessary(query_id=num,system=system,seen_files=seen_files,output=output,runtag=runtag)
        return '\n'.join(output)+"\n"




