from spec import Spec
from tangent.common.utility.math_extractor import MathExtractor
import nose.tools as nt

__author__ = 'Nidhin'


class RegexSplitter_Test(Spec):
    def test_treats_plaintext_okay(self):
        test_input="some input"
        cleaned_input=MathExtractor.input_cleaner(test_input).cleaned_file_content
        nt.eq_(test_input,cleaned_input)

    def test_parses_single_latex(self):
        test_input="$9x$"
        expected_output="<math>9x</math>"
        cleaned_input=MathExtractor.input_cleaner(test_input).cleaned_file_content
        nt.eq_(expected_output,cleaned_input)

    def test_parses_single_latex_with_text(self):
        test_input="$9x$ some say"
        expected_output="<math>9x</math> some say"
        cleaned_input=MathExtractor.input_cleaner(test_input).cleaned_file_content
        nt.eq_(expected_output,cleaned_input)



    def test_parses_string_with_dollar(self):
        test_input="$9 you say . I am not sure "
        cleaned_input=MathExtractor.input_cleaner(test_input).cleaned_file_content
        nt.eq_(test_input,cleaned_input)