import os
from spec import Spec
from tangent.math.models.symboltree import SymbolTree
import nose.tools as nt

__author__ = 'Nidhin'


class MatrixSymbolTree_Test(Spec):
    global base_path
    base_path=os.path.join(os.getcwd(),'tests','test_data')


    def test_symbol_tree_pairs_for_single_elem_matrix(self):
        test_file = os.path.join(base_path,"matrix","single_elem_matrix.mml")

        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, -1)
        symbol_tree = symbol_tree_list[0]

        assert (symbol_tree.latex == "\\begin{pmatrix}5\\end{pmatrix}")
        pairs, paths = symbol_tree.get_pairs(get_paths=True)

        assert len(pairs) == 3

        expected_paths = set(['mstart|mend|1|1', 'matrix|5|0|0', '5|None|0|0'])
        assert len(set(pairs) - expected_paths) == 0



    def test_symbol_tree_pairs_for_single_elem_matrix_and_surrounding_elements(self):
        test_file = os.path.join(base_path,"matrix","single_elem_matrix_surrounded.mml")

        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, -1)
        symbol_tree = symbol_tree_list[0]

        nt.eq_ (symbol_tree.latex , "z=\\begin{pmatrix}5\\end{pmatrix}+1")
        pairs, paths = symbol_tree.get_pairs(get_paths=True)

        assert len(pairs) == 14

        expected_paths = set(['z|1|4|0', 'z|+|3|0', 'mstart|mend|1|1', 'matrix_1_1|+|1|0', '5|None|0|0', '+|1|1|0',
                              '=|matrix_1_1|1|0', 'z|matrix_1_1|2|0', '=|+|2|0', "matrix|5|0|0", 'matrix_1_1|1|2|0',
                              '1|None|0|0', '=|1|3|0', 'z|=|1|0'])



        assert len(set(pairs) - expected_paths) == 0


    def test_symbol_tree_pairs_for_single_elem_matrix_with_none(self):
        test_file = os.path.join(base_path,"matrix","matrix_with_none.mml")

        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, -1)
        symbol_tree = symbol_tree_list[0]

        pairs, paths = symbol_tree.get_pairs(get_paths=True)

        assert len(pairs) == 6
        expected_paths = set(['mstart|mend|2|1',
                              'matrix|i=1|0|0',
                              'i|=|1|0',
                              'i|1|2|0',
                              '=|1|1|0',
                              '1|None|0|0'])
        assert len(set(pairs) - expected_paths) == 0


    def test_symbol_tree_pairs_for_matrix_with_complex_elem(self):
        test_file = os.path.join(base_path,"matrix","matrix_with_sin_cosine.mml")

        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, -1)
        symbol_tree = symbol_tree_list[0]

        pairs, paths = symbol_tree.get_pairs(get_paths=True)

        assert len(pairs) == 138
