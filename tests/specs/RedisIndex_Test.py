import redis

__author__ = 'ntp5633'



from spec import Spec
from tangent.math.models.symboltree import SymbolTree
from tangent.math.index.redisindex import RedisIndex


test_database=5

class RedisIndex_Test(Spec):

    def setup(self):

        #flush the test databse
        r = redis.StrictRedis(db=test_database)
        r.flushdb()

        #create new test database
        self.redis_index = RedisIndex(db=test_database)

        #tests doc has tree douments
        trees, stats = SymbolTree.parse_directory('test_data')
        self.redis_index.add_all(trees)
        self.redis_index.second_pass()


     #   Index i = Index.

    def test_it_creates_the_correct_keys(self):




        r = redis.StrictRedis(db=test_database)

        # 3 expression, 2 docs
        doc_keys=r.keys(pattern="expr:*:doc")

        assert(len(doc_keys)==3)

        # next_expr_id should be 3
        next_id=r.get("next_expr_id")
        assert(next_id=='3')



        #mathmml
        assert(len(r.keys(pattern="expr:*:mathml"))==3)
        #tree:symbol ->expression id
        assert(len(r.keys(pattern="tree:Symbol*"))==3)

        #num of expr_scores
        #7*3+ 3 from expr 0
        assert(len(r.keys(pattern="expr:*score"))==24)


        #correct paths
        assert(len(r.keys(pattern="pair:*paths"))==126)

        #total keys
        assert(len(r.keys(pattern="*"))==296)


        #correct exprs
        assert(len(r.keys(pattern="pair:*exprs"))==126)