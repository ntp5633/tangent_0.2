# -*- coding: utf-8 -*-
from spec import Spec
from tangent.common.utility.math_extractor import MathExtractor
from tangent.math.models.symboltree import SymbolTree
import nose.tools as nt

import os
dir = os.path.dirname(__file__)


class SymbolTree_Test(Spec):
    global base_path
    base_path=os.path.join(os.getcwd(),'tests','test_data')
    def test_it_parses_a_symbol_from_file_correctly(self):

        test_file = os.path.join(base_path,"other","simple.mml")
        a_path=os.path.join(base_path,test_file)
        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, 1)
        symbol_tree = symbol_tree_list[0]

        assert (len(symbol_tree_list) == 1)
        assert (symbol_tree.latex == '2x^{3}+3')


    def test_it_parses_emptyrow(self):
        test_file = os.path.join(base_path,"other","empty_row.mml")

        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, 1)
        symbol_tree = symbol_tree_list[0]

        expexcted_pairs=['empty_row|α|1|1', 'α|None|0|0', 'empty_row|i|1|-1', 'i|None|0|0']
        actual=symbol_tree.get_pairs()

        assert(set(actual)==set(expexcted_pairs))


    def test_it_parses_multiple_symbols_from_file(selfs):

        test_file = os.path.join(base_path,"other","absolute_value.mml")

        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, 1)
        assert (len(symbol_tree_list) == 2)
        #first symbol from file

        extracted_latex=set()
        for s in symbol_tree_list:
            assert isinstance(s,SymbolTree)
            extracted_latex.add(s.latex)

        assert (len(extracted_latex) == 2)

        assert '\\left|p^{n}\\frac{a}{b}\\right|_{p}=p^{{-n}}.' in extracted_latex
        assert '|x/y|=|x|/|y|.\,' in extracted_latex


    def test_it_parses_xml_with_no_alt_text(selfs):
        test_file = os.path.join(base_path,"other","mmml_no_altext.mml")

        moutput = MathExtractor.parse_from_xml(test_file)
        math_token_positions=moutput.math_token_positions
        latex_mathml=moutput.latex_mathml_dict.keys()
        assert (len(math_token_positions) == 1)
        assert (len(latex_mathml) == 1)

        assert '{}^{{235}}' in math_token_positions
        assert '{}^{{235}}' in latex_mathml




    def test_it_generates_correct_symbol_tree_pairs(self):
        test_file = os.path.join(base_path,"other","simple.mml")

        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, 1)
        symbol_tree = symbol_tree_list[0]

        assert isinstance(symbol_tree, SymbolTree)
        tuple_pairs = symbol_tree.get_pairs(get_paths=True)

        assert isinstance(tuple_pairs, tuple)
        assert len(tuple_pairs) == 2

        pairs, paths = tuple_pairs
        assert (len(pairs) == 10)
        true_pairs = {'2|x|1|0',
                      '2|3|2|1',
                      '2|+|2|0',
                      '2|3|3|0',
                      'x|3|1|1',
                      'x|+|1|0',
                      'x|3|2|0',
                      '+|3|1|0',
                      '3|None|0|0',
                      '3|None|0|0'
        }

        intersection = set(pairs) - true_pairs
        assert len(intersection) == 0, "Contains all the expected pairs and distance"


    def test_it_generates_correct_symbol_tree_pairs_for_frac(self):
        test_file = os.path.join(base_path,"other","frac.mml")


        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, 1)
        symbol_tree = symbol_tree_list[0]

        assert (symbol_tree.latex == '\\frac{x}{y}')
        assert isinstance(symbol_tree, SymbolTree)
        tuple_pairs = symbol_tree.get_pairs(get_paths=True)

        assert isinstance(tuple_pairs, tuple)
        assert len(tuple_pairs) == 2

        pairs, paths = tuple_pairs
        assert (len(pairs) == 4)
        true_pairs = {'frac|x|1|1',
                      'frac|y|1|-1',
                      'x|None|0|0', 'y|None|0|0'
        }

        intersection = set(pairs) - true_pairs
        assert len(intersection) == 0, "Contains all the expected pairs and distance"






