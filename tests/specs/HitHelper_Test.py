from tangent.common.hits_generation.modified_hit_generator import SentenceGraphFragmentGenerator, SentenceGraph
from tangent.common.utility.math_extractor import MathExtractor
from tangent.common.hits_generation.position_scorer import NaivePoistionSocrer

__author__ = 'Nidhin'

import codecs
from spec import Spec

import nose.tools as nt


class HitHelper_Test(Spec):
    def test_it_provides_poistion_score_for_one_text_token(self):
        test_file = "test_data/math0006179_para_3.xhtml"

        query_tokens = ["good"]
        token_postions = {"good": [0, 5]}
        score, score_reason = NaivePoistionSocrer.calculate_position_score(query_tokens, token_postions)

        assert score == 1
        assert score_reason == "one token"


    def test_it_provides_poistion_score_for_one_math_token(self):
        test_file = "test_data/math0006179_para_3.xhtml"

        query_tokens = ["\\frac{x}{y}"]
        token_postions = {"\\frac{x}{y}": [0, 5]}
        score, score_reason = NaivePoistionSocrer.calculate_position_score(query_tokens, token_postions)

        assert score == 1
        assert score_reason == "one token"

    def test_it_provides_position_score_for_non_match(self):
        query_tokens = ["bad"]
        token_postions = {"\\frac{x}{y}": [0, 5]}
        score, score_reason = NaivePoistionSocrer.calculate_position_score(query_tokens, token_postions)

        assert score == 0
        assert score_reason == "no match"


    def test_it_provides_Score_token_pair_not_matched(self):
        query_tokens = ["bad", "good"]
        token_postions = {"bad": [0, 5], "sad": [0, 5]}
        score, score_reason = NaivePoistionSocrer.calculate_position_score(query_tokens, token_postions)

        nt.assert_equal(score, 0)
        # nt.assert_equal(score_reason, 'positions:{"bad": [0, 5], "sad": [0, 5]};scores:[0]')

    def test_it_provides_correct_hit(self):
        path = "..\\test_data\\f000001.xhtml"

        content = ""
        with codecs.open(path, 'r', "utf-8") as myfile:
            content = myfile.read().replace('\n', ' ')

        moutput = MathExtractor.input_cleaner(content)
        # generate_snippet(content)
        token_positions = {}
        token_positions[u'\\phi _{{c,{\\rm max}}}\\sim\\frac{gT}{\\lambda}\\,, '] = [1730]
        token_positions["Coleman-Weinberg"] = [1436]
        token_positions["type"] = [1437]


    def test_modified_hit_generator(self):
        path = "test_position\\brain_chip.txt"
        contents = SentenceGraphFragmentGenerator.generate_sentence_graph(path=path)
        query = "Brain Chip Research"
        cleaned_query = SentenceGraph.tokenize_and_stem(query)
        summary = SentenceGraphFragmentGenerator.generate_snippet(cleaned_query, contents)
        expected_summary = "Brain chip offers hope for paralyzed Donoghue's initial research, published in the science journal Nature in 2002, consisted of attaching an implant to a monkey's brain that enabled it to play a simple pinball computer game remotely."
        expected_summry2='Brain chip offers hope for paralyzed The chip, called BrainGate, is being developed by Massachusetts-based neurotechnology company Cyberkinetics, following research undertaken at Brown University, Rhode Island.'
        nt.eq_(summary,expected_summry2)


    def test_modified_hit_generator_simple(self):
        path = "test_position\\simple.txt"
        contents = SentenceGraphFragmentGenerator.generate_sentence_graph(path=path)
        query = "Brain Chip Research"
        cleaned_query = SentenceGraph.tokenize_and_stem(query)
        summary = SentenceGraphFragmentGenerator.generate_snippet(cleaned_query, contents)

        expected_summary = "Brain chip offers hope for paralyzed Donoghue's initial research, published in the science journal Nature in 2002, consisted of attaching an implant to a monkey's brain that enabled it to play a simple pinball computer game remotely."

        nt.eq_(expected_summary,summary)

    def test_modified_hit_generator_non_match(self):
        path = "test_position\\simple.txt"
        contents = SentenceGraphFragmentGenerator.generate_sentence_graph(path=path)
        query = "bad cat"
        cleaned_query = SentenceGraph.tokenize_and_stem(query)
        summary = SentenceGraphFragmentGenerator.generate_snippet(cleaned_query, contents)

        expected_summary = ""


        nt.eq_(expected_summary,summary)

    def test_modified_hit_generator_partial_match(self):
        """
        Doc has words ,but there is no real mathch
        :return:
        """
        path = "test_position\\partial_match.txt"
        contents = SentenceGraphFragmentGenerator.generate_sentence_graph(path=path)
        query = "brain chip research"
        cleaned_query = SentenceGraph.tokenize_and_stem(query)
        summary = SentenceGraphFragmentGenerator.generate_snippet(cleaned_query, contents)

        expected_summary = "brain"

        nt.eq_(expected_summary,summary)