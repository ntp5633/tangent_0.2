import os
from spec import Spec

from tangent.common.utility.math_extractor import MathExtractor


__author__ = 'Nidhin'


class Math_Test(Spec):
    global base_path
    base_path=os.path.join(os.getcwd(),'tests','test_data')

    def test_it_parses_html_correctly(self):
        test_file = os.path.join(base_path,"math0006179_para_3.xhtml")

        content = ""
        with open(test_file,encoding="utf8") as myfile:
            content = myfile.read().replace('\n', '')


        moutput=MathExtractor.input_cleaner(content)
        ( cleaned_input, latex_mathml) = moutput.cleaned_file_content,moutput.latex_mathml_dict

        expected_input = u"xml version=\\'1.0\\' encoding=\\'%SOUP-ENCODING%\\'A similar result for $so_{q}(3)$ has been obtained in[5], where itsgenerators have been constructed in terms of generators of $so(3)$."

        assert (cleaned_input, expected_input)
        assert (len(latex_mathml.keys()) == 2)


    def test_it_provides_correct_tokens(self):
        test_file = os.path.join(base_path,"math0006179_para_3.xhtml")
        content = ""
        with open(test_file,encoding="utf8") as myfile:
            content = myfile.read().replace('\n', '')

        moutput=MathExtractor.input_cleaner(content)
        actual_token_positions=moutput.math_token_positions
        expected_token_positions = [(u'so_{q}(3)', 6), (u'so(3)', 21)]
        assert (actual_token_positions, expected_token_positions)


    def test_it_parses_math_expressions_correctly(self):
        test_file = os.path.join(base_path,"math0006179_para_3.xhtml")

        moutput=MathExtractor.input_cleaner(test_file)

        actual_token_positions = moutput.math_token_positions
        expected_token_positions = [(u'so_{q}(3)', 6), (u'so(3)', 21)]
        assert (actual_token_positions, expected_token_positions)


    def test_it_parses_math_expressions_correctly_2(self):
        test_file = os.path.join(base_path,"astro-ph0007170_para_27.xhtml")


        moutput=MathExtractor.input_cleaner(test_file)

        actual_token_positions = moutput.math_token_positions
        expected_token_positions = [(u'so_{q}(3)', 6), (u'so(3)', 21)]
        assert (actual_token_positions, expected_token_positions)