import os
from spec import Spec
from tangent.math.models.symboltree import SymbolTree

__author__ = 'Nidhin'


class MultiscriptSymbolTree_Test(Spec):
    global base_path
    base_path=os.path.join(os.getcwd(),'tests','test_data')

    def test_it_parses_a_complete_one(self):
        test_file = os.path.join(base_path,"multiscripts","complete.mml")
        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, 1)
        symbol_tree = symbol_tree_list[0]
        actual_pairs = symbol_tree.get_pairs()

        expected_pairs = ['multiscript|a|1|1', 'multiscript|X|2|0', 'multiscript|c|3|1', 'multiscript|d|3|-1',
                          'a|X|1|-1', 'a|c|2|0', 'a|d|2|-2', 'X|c|1|1', 'c|None|0|0', 'X|d|1|-1', 'd|None|0|0',
                          'multiscript|b|1|-1', 'multiscript|X|2|0', 'multiscript|c|3|1', 'multiscript|d|3|-1',
                          'b|X|1|1', 'b|c|2|2', 'b|d|2|0', 'X|c|1|1', 'c|None|0|0', 'X|d|1|-1', 'd|None|0|0']

        assert (set(actual_pairs) == set(expected_pairs))


    def test_it_parses_a_none(self):
        test_file = os.path.join(base_path,"multiscripts","none.mml")
        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, 1)
        symbol_tree = symbol_tree_list[0]
        actual_pairs = symbol_tree.get_pairs()

        expected_pairs = ['multiscript|b|1|-1', 'multiscript|X|2|0', 'multiscript|c|3|1', 'b|X|1|1', 'b|c|2|2', 'X|c|1|1', 'c|None|0|0']
        assert (set(actual_pairs) == set(expected_pairs))


    def test_it_parses_one_doesnt_use_none(self):
        test_file = os.path.join(base_path,"multiscripts","not_using_none.mml")

        symbol_tree_list = SymbolTree.parse_all_from_xml(test_file, 1)
        symbol_tree = symbol_tree_list[0]
        actual_pairs = symbol_tree.get_pairs()

        expected_pairs = ['multiscript|X|1|1', 'multiscript|A|2|0', 'X|A|1|-1', 'A|None|0|0']
        assert (set(actual_pairs) == set(expected_pairs))
