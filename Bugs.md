Known bugs, issues, limitations
=========

# Web Frontend
Currently, the way the applications are deployed is the api layer is run on a different port
The web interface are run on another server, and maker cors request ot the backend
 
One way to fix this is to have both the api and web interface bundled together.
An alternative, is to serve the backend and frontend behind a proxy layer


The tangent component, embeds min in the webinterface using a cors call.


# Fragment Generation
Code resides in tangent\common\modified_hit_generator.

For some documents, the length of the generated fragment is longer than expected. 
The resaon is due to not being able to detect sentence boundaries in the original document especially if the docuemnt is xml based

#Ranking
It was noticed when comparing a math query expression to  a an exact match in an indexed xml document, the score was not one.]
One possible reason for this is that the parsed equation from the xml document does not match the same equation.

Before a document is tokenized, the document is preprocessed with Beautiful Soup (stripping tags and getting content inside it)

#Position Scorer
The code for ranking position of a query with respect to a retrieved documents resides in tangent\common\hit_generator\position_scorer.py

Sometimes incorrect position scores are returned, due to possibly incosistent stemming of the query and during indexing
