# Tangent 
---

Version 0.2  
Copyright (c) 2013-2014 David Stalnaker, Nidhin Pattaniyil, and Richard Zanibbi

[Document and Pattern Recognition Lab](http://www.cs.rit.edu/~dprl)  
Department of Computer Science, Rochester Institute of Technology  
Rochester, New York, USA

Authors:

* David Stalnaker: <david.stalnaker@gmail.com> (version 0.1, August 2013) 
* Nidhin Pattaniyil: <npatta01@gmail.com> (version 0.2, September 2014, for NTCIR-11 Math-2)
* Richard Zanibbi: <rlaz@cs.rit.edu> (co-designer/advisor)

*Tangent* is distributed under a GPL License.
Please consult the LICENSE file in this directory for more information.






# Overview
---

Tangent is a math search engine created by David Stalknaker and later extended by Nidhin Pattaniyil during their Masters' studies in Computer Science at the Rochester Institute of Technology (Rochester, New York, USA). The most recent version supports searching for a combination of one or more mathematical expressions along with keywords. Expressions represented in LaTeX and Presentation MathML may be indexed.
Text in 
documents is indexed using Solr/Lucene. Math expressions are 
indexed using the relative positions of symbol pairs in an
expression, and then stored in Redis (an in-memory hash table), 
and later MySql (on disk).

Details about the system design and implementation may be found in the publications below.


* [D. Stalnaker (2013) Math Expression Retrieval Using Symbol Pairs in Layout
Trees. Master's Thesis, Rochester Institute of Technology (Computer Science),
NY, USA (August 2013)](http://www.cs.rit.edu/~dprl/files/StalnakerMScThesisAug2013.pdf)

* Stalnaker, D. and Zanibbi, R. (2015) Math expression retrieval using an inverted index over symbol pairs. Proc. Document Recognition and Retrieval, San Francisco (to appear).

* Pattaniyil, N. and Zanibbi, R. (2014) Combining TF-IDF Text Retrieval with an Inverted Index over Symbol Pairs in Math Expressions: The Tangent Math Search Engine at NTCIR 2014. Proc. 11th NII Testbeds and Community for Information access Research (NTCIR), Tokyo, Japan (to appear, Dec. 2014).

 
## Versions


The versions below have been tagged in the git repository.

####Version 0.1 
- Experimentation with different methods for tokenizing expressions and ranking hits
 (symbol pair f-measure scoring was found to be effective)  
- Custom math index using Redis in-memory hash table

####Version 0.2 
 - Modification to index additional structures in math expressions  	* e.g. matrices, and prefix subscripts/superscripts
 - Inclusion of text based indexing using Solr - expressions are converted to placeholder identifiers (i.e. expression structure is not represented in the text index)
 - Migration from Redis to MySQL to accomodate large document collections
 





# Installation and Dependencies
---

Below are the external components used by Tangent.

* [Latexml](http://dlmf.nist.gov/LaTeXML/index.html): a library to covert a Text expression to MathMl
* [MathJax](http://www.mathjax.org/): a javascript library used in displaying the math expressions in the results page
* python 3.3

Python Modules:

* [werkzeug](http://werkzeug.pocoo.org/):WSGI Utility Library
* [flask](http://flask.pocoo.org/): Microframework for Python
* [pymysql](https://github.com/PyMysql/PyMysql): MySQL binding
* [nltk](http://www.nltk.org/): Natural LanguageToolkit
* [simplejson](): json library
* [redis](https://pypi.python.org/pypi/redis/): Python redis
* [pysolr](https://github.com/toastdriven/pysolr/): Python interface to talk to Solr
* beautifulsoup4 : Xml parsing library

Using *pip*, the above python modules can be installed using the command:

    pip install flask pymysql nltk simplejson redis pysolr beautifulsoup4 flask-cors jsonnpickle networkx


MySql:

-   The index stored for math tokens
-   Sample Schema is found in the configuration\database_ntcir folder
-   Assumes there is a user/password with the
	'math_searcher','KEfks5sxIYAA7z7QnNkg' that has all permissions for the
	created databases

Solr:

  - The index used to store the text index is solr. 
  - Solr rep and code can be found in the following [repository.](https://ntp5633@bitbucket.org/ntp5633/solr.git)



# Indexing a Collection
-----------------

To index a collection, a file needs to be provided that lists all the files to
be indexed and the corresponding document ids.  Given a directory, the mapping
file can be created using the file
"tangent\common\utility\create_document_ids.py"

For the included sample, the follwing command may be used:

    python tangent\common\utility\create_document_ids.py sample generic filemapping_small.txt
    
    
### Creating the Math Index


Assuming there is a mysql database called 'math_ntcir' and filemapping file
'filemapping_small', the following snippet creates the math index:

    python tangent\math\math_indexer.py index filemapping_small.txt math_ntcir
 
To create the text index:

    python tangent/text/text_indexing.py <collection> filemapping_small.txt
    
        where <collection> is a solr collection (folder in solr/actual/solr)
        recognized <collection> is ntcir, simple, or mrec



### Running the Webserver

Using the following, the server will launch and be available on the port defined in the config object.

    python tangent\server\mathsearch.py config_object
        config_object: class name of Config object; ex: config.FMeasureConfig




To run the actual user interface, navigate to the webapp folder and issue the following: 
        
        python -m http.server
    



# NTCIR-11 Notes
----

Ntcir-11 main and wikipedia task queries can be found in tangent\ntcir directory, in the files 'ntcir11_queries.xml' and 'ntcii-wikipedia.xml'. Our results are provided separately in a file 'ntcir_results_2011.zip' provided separately on the dprl lab web pages (dprl lab link is at the bottom of this file).

For the ntcir task, we split the filemapping document into nine pieces using the following command.
        
       split --lines=1000000 filemapping_small.txt part_
    
The above command should create nine files containing 1 million documents each.
There were also nine databases that had the same schema named as *math_ntcir_aa*, *math_ntcir_ab*, etc.
To index the math collection instead of passing *filemapping_small*, pass *part_aa*.
    
    python tangent\math\math_indexer.py index part_aa.txt math_ntcir_aa

To obtain search results:

    python tangent\ntcir\ntcir11.py <name of run> <weighting stratergy> <ntcir queriy file>  <system> 
         
         <name of run> : can be anything 
         <weighting strategy> : 'math_only,''math_focused','ntcir_default', 'math_text_equal' 
         <system>: 'Wikipedia', 'Ntcir'



#Important Files and Directories
---


   * **math indexing:** see directory tangent\math\index 
   * **text indexing:** see directory tangent\text\index
   * **retrieval:** Class responsible for searching the indices is defined in the file tangent\server\
   * **web front-end:** The web component, consists of the api layer and the web assets.
  
  	* API layer
    
   		- Is just a python flask backend
   		- Found in in matsearch.py in tangent\server
  		- Main endpoint is the method combined_search_api

  	* Web assets
  
  		- The assets can be found in the folder webapp
  		- The assets need to served from the base endpoint tangent2 (using a proxy server, or rename the directory)
      



* * *

Visit the [DPRL page](http://www.cs.rit.edu/~dprl/) for more information about the lab and projects.

This material is based upon work supported by the National Science Foundation under Grant No. IIS-1016815.
Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s)
and do not necessarily reflect the views of the National Science Foundation.