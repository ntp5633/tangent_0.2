MathJax.Hub.Config({skipStartupTypeset: true});
MathJax.Hub.Configured();

var myApp = angular.module("myApp", ['ngRoute', 'ngAnimate']);

myApp.directive("mathjaxBind", function () {
    return {
        restrict: "A",
        controller: ["$scope", "$element", "$attrs", function ($scope, $element, $attrs) {
            $scope.$watch($attrs.mathjaxBind, function (value) {
                $element.text(value == undefined ? "" : value);
                MathJax.Hub.Queue(["Typeset", MathJax.Hub, $element[0]]);
            });
        }]
    };
});


myApp.directive('customiframe', function ($log, $compile) {
    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        scope: {
            url: '@'
        },
        template: '<iframe ng-src="{{ url }}"></iframe>',
        //template: '<iframe src="http://localhost:8080/min/index.xhtml?requery=5x"></iframe>',

        link: function (scope, element, attrs) {

            element.load(function () {

                var address="/tangent2/styles/iframe.css";
                var address="/styles/iframe.css";
                var $head = $(this).contents().find("head");
                $head.append($("<link/>",
                    { rel: "stylesheet", href:address , type: "text/css" }));


            });


        }
    };
});


myApp.directive('fluidvids', function ($log) {
    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        scope: {
            video: '@'
        },
        template: '<div class="fluidvids">' +
            '<iframe ng-src="{{ video }}"></iframe>' +
            '</div>',
        link: function (scope, element, attrs) {
            var ratio = (attrs.height / attrs.width) * 100;
            element[0].style.paddingTop = ratio + '%';

            $log.log(ratio);
        }
    };
});


myApp.controller('MyCtrl', ['$scope', '$element', '$location','$rootScope','min_service', function ($scope, $element, $location,$rootScope,min_service) {
    $scope.query = "";
    $scope.query2 = "some query 2";
    $scope.hitlist = [];
    //$scope.hitlist[0] = "\\(\\frac{5}{4} \\div \\frac{1}{6}\\) so said the cat";
    //$scope.hitlist[1] = "7x";
    //$scope.hitlist[2] = "\\(5x_{2}\\)";

    $scope.tokens = [];


    var _this=this;




    $scope.drawExpression = function () {
        $scope.selectedIndex = -1;
        var hardcodedlates='$5x$';
        $location.path('/expression/'+hardcodedlates )




    };

    $rootScope.$on('added_expression',function(){
        token=min_service.new_query ;

        aa = new Object()
        aa.original = '$'+token+'$';
        aa.rendered = "\\(" + token + "\\)";

        if ($scope.selectedIndex==-1){
            $scope.tokens.push(aa);
            $scope.query= $scope.query + " " +aa.original
        }else{
            $scope.tokens[$scope.selectedIndex].original=aa.original
            $scope.tokens[$scope.selectedIndex].rendered=aa.rendered

            var arrayLength = $scope.tokens.length;
            var _query="";
            for (var i = 0; i < arrayLength; i++) {
                var token = $scope.tokens[i];
                _query=_query+" "+token.original;

            }
            $scope.query=_query

        }




    });

    $scope.data_sources = [
        {system: 'NTCIR Actual'},
        {system: 'NTCIR Test'},
        {system: 'MREC'},
        {system: 'Wikipedia'}
    ];


    $scope.weights = [
        {weight: 'math_only'},
        {weight: 'math_focused'},
        {weight: 'ntcir_default'},
        {weight: 'math_text_equal'}
    ];

    $scope.selected_datasource = $scope.data_sources[0];
    $scope.selected_weight=$scope.weights[0];

    var i = 0;
    $scope.change = function () {


        $scope.tokens = [];
        if ($scope.query == null || $scope.query.trim() == "") {
            $scope.tokens.length = 0;
            return;
        }
        var tokenized = $scope.query.match(/\$.*?\$|\S+/g);

        if (tokenized == null) {
            $scope.tokens.length = 0;
        } else {
            var arrayLength = tokenized.length;
            for (var i = 0; i < arrayLength; i++) {
                var token = tokenized[i];
                last_char = token.substr(token.length - 1);
                first_char = token.charAt(0);
                $scope.tokens[i] = new Object();
                $scope.tokens[i].original = token;
                $scope.tokens[i].rendered = token;
                if (first_char == '$' && last_char == '$') {
                    token_without_stars = token.substring(1, token.length - 1);
                    $scope.tokens[i].rendered = "\\(" + token_without_stars + "\\)";
                }


            }
        }


    };

    $scope.selectedIndex = -1;
    $scope.processClick = function (selected) {
        if (selected == $scope.selectedIndex) {
            $scope.selectedIndex = -1;
            $location.path('/');
        } else {
            if ($scope.tokens[selected].original == $scope.tokens[selected].rendered) {
                $scope.selectedIndex = -1
            } else {
                var latex = $scope.tokens[selected].original;
                $scope.selectedIndex = selected;
                $location.path('/expression/' + latex)
            }

        }

    };

    $scope.performSearch = function () {
        var selected_datasource = $scope.selected_datasource.system;
        var search_query = $scope.query;
        var selected_weight=$scope.selected_weight.weight;
        $location.path('/results').search({ query: search_query, system: selected_datasource,weight:selected_weight})


    };


}]);

myApp.controller('ResultsCtrl', ['$scope', '$routeParams', 'tangent_service', '$rootScope', function ($scope, $routeParams, tangent_service, $rootScope) {
    this.name = "ResultsCtrl";
    this.params = $routeParams;

    this.system = this.params.system;
    this.query = this.params.query;
    this.weight = this.params.weight;

    this.result_info = {
        num_results: 0,
        num_math_results: 0,
        num_text_results: 0

    };

    this.result_info = {
        num_results: 0,
        num_math_results: 0,
        num_text_results: 0

    };
    this.results = [];

    this.results_available = false;

    var _this = this;
    $scope.update_results = function (response) {
        var parsed_response = response;
        _this.num_results = parsed_response.shared_results;
        _this.num_math_results = parsed_response.num_math_results;
        _this.num_text_results = parsed_response.num_text_results;

        _this.result_info['num_results'] = parsed_response.num_results;
        _this.result_info['num_text_results'] = parsed_response.num_text_results;
        _this.result_info['num_math_results'] = parsed_response.num_math_results;

        _this.results_available = true;
        _this.time_taken = parsed_response.time_taken;
        //this.results.length=0;
        //this.results.push.apply(this.results,parsed_response.results);
        //Array.prototype.push.apply($scope.results, parsed_response.hit_list);
        //Array.prototype.push.apply(this.results, parsed_response.hit_list);
        _this.results = parsed_response.hit_list;
        //$scope.results=parsed_response.results;


        var re = new RegExp("<math.*?>(.*?)</math>", "g");
        for (var i = 0; i < _this.results.length; i++) {

            var frag = _this.results[i].fragment;
            if (frag != null) {
                _this.results[i].fragment = _this.results[i].fragment.replace(re, "\\($1\\)");
            }

        }
    };

    tangent_service.getResults(this.query, this.system,this.weight).success($scope.update_results);


}]);


myApp.controller('MathOnlyResultsCtrl', ['$routeParams', 'tangent_service', function ($routeParams, tangent_service) {
    this.name = "MathOnlyResultsCtrl";
    this.params = $routeParams;
    //this.latex = this.params.latex;
    this.latex = this.params.latex.substring(1, this.params.latex.length - 1);


    tangent_service.getMathOnlyResults(function (response) {
        var parsed_response = JSON.parse(response);
        this.num_results = parsed_response.num_results;
        this.time_taken = parsed_response.time_taken;
        this.results = parsed_response.results;


    });

}]);


myApp.controller('MinCtrl', ['$routeParams', '$sce', 'min_service', function ($routeParams, $sce, min_service) {
    this.name = "MinCtrl";
    this.params = $routeParams;
    //this.latex = this.params.latex;
    this.latex = this.params.latex.substring(1, this.params.latex.length - 1);
    //this.src = 'http://localhost:8080/min/index.xhtml?requery=5x'
    //this.src = '/min/index.xhtml?requery=' + this.latex;
    //this.src = 'http://localhost:8080/min/index.xhtml?requery=' + this.latex;
    //this.src = 'http://saskatoon.cs.rit.edu/min/index.xhtml?requery=' + this.latex;
    this.src = '../min/index.xhtml?requery=' + this.latex;

    this.addExpression = function () {

        try {
            var text=$("#min_interface iframe").contents().find(".MathJax_Display").next().text();

            var cleaned_text=text.replace(/ /g, '')
            min_service.addExpressionToQuery(cleaned_text);
        } catch (e) {

        }


    }

}]);


myApp.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/results', {
                templateUrl: 'templates/math_results.html',
                controller: 'ResultsCtrl',
                controllerAs: 'res'
            })
            .when('/expression/:latex', {
                templateUrl: 'templates/min.html',
                controller: 'MinCtrl',
                controllerAs: 'min'
            })
            .when('/mathsearch', {
                templateUrl: 'templates/math_results.html',
                controller: 'MathOnlyResultsCtrl',
                controllerAs: 'mrc'
            })
            .when('/',{
                controller : 'MyCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });
           ;

        // configure html5 to get links working on jsfiddle
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
    }]);


myApp.config(function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;

    //Remove the header used to identify ajax call  that would prevent CORS from working
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
});

myApp.service('tangent_service', function ($http) {
    delete $http.defaults.headers.common['X-Requested-With'];
    //this.base_url = 'http://localhost/tangentapi';
    this.base_url='http://saskatoon.cs.rit.edu:9997/tangentapi';
    this.base_url='http://ec2-54-91-35-183.compute-1.amazonaws.com:9997/tangentapi';
    this.base_url='http://localhost:9997/tangentapi';
    //this.base_url='http://saskatoon.cs.rit.edu:9997/tangentapi';
//    this.base_url='http://localhost:9997/tangentapi';
    this.getMathOnlyResults = function (latex_query, callbackFunc) {
        $http({
            method: 'GET',
            url: this.base_url + '/math_only_results',
            params: 'q=' + latex_query

        }).success(function (data) {
            // With the data succesfully returned, call our callback
            callbackFunc(data);
        }).error(function () {
            alert("error");
        });
    };


    this.getResults = function (query_str, system,weight) {
        return $http({
            method: 'GET',
            url: this.base_url + '/combined/search',
            params: {query: query_str, system: system,weight:weight}

        });
    }


});


myApp.service('min_service', function ($rootScope) {

    this.new_query = "";

    var _this = this;
    this.add_new=false;

    this.addExpressionToQuery = function (latex,new_expr) {
        if (latex != null && latex != "") {
            _this.new_query = latex;

            _this.add_new=new_expr;
            $rootScope.$broadcast('added_expression');
        }
    };


});


myApp.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'http://localhost:8080/**',
        'http://localhost:9002/**',
        'http://localhost:8080/min/**',
        'http://saskatoon.cs.rit.edu/**',
        'http://saskatoon.cs.rit.edu:9999/**',
        'http://saskatoon.cs.rit.edu:9997/**',
        'http://ec2-54-91-35-183.compute-1.amazonaws.com/**',
        'http://www.youtube.com/**'
    ]);

    // The blacklist overrides the whitelist so the open redirect here is blocked.

});

	
